<?

class ReplicateB2BCommand extends CConsoleCommand
{
	private $models = array(
		'MarketUser',
		'MarketItem',
		'MarketCurrency',
		'MarketItemPrice',
		'MarketItemAvailability',
        'MarketWarehouse',
	);

	public function run($args)
	{
		foreach ($this->models as $modelName)
		{
			$model = new $modelName();

			$replication = MarketReplication::model()->findByAttributes(array('table_name' => $model->tableName()));
			if (!$replication)
			{
				$replication = new MarketReplication();
				$replication->table_name = $model->tableName();
				$replication->max_update_time = 0;
			}
			$replication->max_update_time = $model->replicate($replication->max_update_time);
			$replication->save();
		}
	}
}

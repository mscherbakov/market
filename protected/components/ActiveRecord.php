<?

class ActiveRecord extends CActiveRecord
{
    private $_transaction;

    public function beginTransaction()
    {
        if (!Yii::app()->db->getCurrentTransaction())
            $this->_transaction = Yii::app()->db->beginTransaction();
    }

    public function rollbackTransaction()
    {
        if ($this->_transaction)
        {
            $this->_transaction->rollback();
            $this->_transaction = null;
        }
    }

    public function commitTransaction()
    {
        if ($this->_transaction)
        {
            $this->_transaction->commit();
            $this->_transaction = null;
        }
    }

    public function delete()
    {
        $result = parent::delete();
        if (!$result)
            $this->rollbackTransaction();
        return $result;
    }

    protected function afterDelete()
    {
        $this->commitTransaction();
        parent::afterDelete();
    }
}

?>
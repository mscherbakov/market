<?
/**
 * Author: MaxHero
 * Date: 23.08.11
 * Time: 11:54
 */

class ArrayValidator extends CValidator
{
	public $validatorName;
	public $validatorParams = array();

	/**
	 * Validates a single attribute.
	 * This method should be overridden by child classes.
	 * @param CModel $object the data object being validated
	 * @param string $attribute the name of the attribute to be validated.
	 */
	protected function validateAttribute($object, $attribute)
	{
		if (!is_array($object->$attribute))
		{
			throw new CException("object passed into ArrayValidator must be an array");
		}

		$validator = self::createValidator($this->validatorName, $object, array($attribute), $this->validatorParams);

		$className = get_class($object);
		$errors = array();
		foreach ($object->$attribute as $key => $value)
		{
			$tmpObject = new $className();
			$tmpObject->$attribute = $value;
			$validator->validateAttribute($tmpObject, $attribute);

			if ($tmpObject->hasErrors())
			{
				$errors[$key] = array();
				foreach ($tmpObject->errors[$attribute] as $error)
				{
					$errors[$key][] = $error;
				}
				$errors[$key]["index"] = $key;
			}
		}
		$object->addErrors(array($attribute => $errors));
	}

}

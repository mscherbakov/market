<?php

class AuthorBehavior extends CActiveRecordBehavior
{
    public $authorAttribute = 'author_id';
    public $editorAttribute = 'editor_id';
    public $setEditorOnCreate = false;

    public function beforeSave($event)
    {
        $userId = null;
        if (Yii::app() instanceof CWebApplication && !Yii::app()->getUser()->getIsGuest())
            $userId = Yii::app()->getUser()->getId();

        if ($this->getOwner()->getIsNewRecord() && ($this->authorAttribute !== null))
        {
            $this->getOwner()->{$this->authorAttribute} = $userId;
        }
        if ((!$this->getOwner()->getIsNewRecord() || $this->setEditorOnCreate) && ($this->editorAttribute !== null))
        {
            $this->getOwner()->{$this->editorAttribute} = $userId;
        }
    }
}
<?

class CategoryValidator extends CValidator
{
    protected function validateAttribute($object, $attribute)
    {
        $model = Category::model()->findByPk($object->{$attribute});
        if ($model && count($model->children) > 0)
            $object->addError($attribute, 'Категория содержит подкатегории');
    }
}

<?

class FilterObject extends CComponent
{
	/**
	 * @var string
	 */
	private $_id;

	/**
	 * @var string
	 */
	private $_name;

	/**
	 * @var FilterOptionsCollection
	 */
	private $_options;

	/**
	 * @var FilterType
	 */
	private $_type;

	/**
	 * @var int
	 */
	private $_rangeMinValue;

	/**
	 * @var int
	 */
	private $_rangeMaxValue;

	/**
	 * @var int
	 */
	private $_currentMinValue;

	/**
	 * @var int
	 */
	private $_currentMaxValue;

	/**
	 * @var string
	 */
	private $_value;

	public static function fromModel(Filter $model)
	{
		$result = new self($model->id, $model->name, $model->type_id);

		switch ($model->type_id)
		{
			case FilterType::INTEGER:
				$result->rangeMinValue = $model->getMinValue();
				$result->rangeMaxValue = $model->getMaxValue();
				break;
			case FilterType::STRING:
				foreach ($model->getValuesListData() as $optionId => $optionValue)
				{
					$result->getOptions()->Add(new FilterOption($optionId, $optionValue));
				}
				break;
		}

		return $result;
	}

	public function __construct($id, $name, $type)
	{
		$this->_id = $id;
		$this->_name = $name;
		$this->_type = FilterType::findByPk($type);

		if ($this->_type->getId() == FilterType::STRING)
		{
			$this->_options = new FilterOptionsCollection();
		}
		elseif ($this->_type->getId() == FilterType::INTEGER)
		{
			if (isset($_GET[$this->_id . "_minValue"]) && !empty($_GET[$this->_id . "_minValue"]))
			{
				$this->_currentMinValue = (int)$_GET[$this->_id . "_minValue"];
			}
			if (isset($_GET[$this->_id . "_maxValue"]) && !empty($_GET[$this->_id . "_maxValue"]))
			{
				$this->_currentMaxValue = (int)$_GET[$this->_id . "_maxValue"];
			}
		}

		if (isset($_GET[$this->_id]))
		{
			$this->_value = $_GET[$this->_id];
		}
	}

	/**
	 * @param array $actionParams
	 * @return void
	 */
	public function unsetParams(array &$actionParams)
	{
		unset($actionParams[$this->_id]);

		if ($this->_type->getId() == FilterType::INTEGER)
		{
			unset($actionParams[$this->_id . "_minValue"]);
			unset($actionParams[$this->_id . "_maxValue"]);
		}
	}

	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->_id;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->_name;
	}

	public function getType()
	{
		return $this->_type;
	}

	public function getValue()
	{
		if ($this->_type->getId() != FilterType::BOOLEAN)
			return null;

		return $this->_value;
	}

	/**
	 * @return FilterOptionsCollection
	 */
	public function getOptions()
	{
		if ($this->_type->getId() != FilterType::STRING)
			return null;

		return $this->_options;
	}

	/**
	 * @param int $value
	 */
	public function setRangeMaxValue($value)
	{
		if ($this->_type->getId() != FilterType::INTEGER)
			return;

		$this->_rangeMaxValue = $value;
		if ($this->_currentMaxValue == null)
		{
			$this->_currentMaxValue = $value;
		}
	}

	/**
	 * @return int
	 */
	public function getRangeMaxValue()
	{
		if ($this->_type->getId() != FilterType::INTEGER)
			return;

		return $this->_rangeMaxValue;
	}

	/**
	 * @param int $value
	 */
	public function setRangeMinValue($value)
	{
		if ($this->_type->getId() != FilterType::INTEGER)
			return;

		$this->_rangeMinValue = $value;
		if ($this->_currentMinValue == null)
		{
			$this->_currentMinValue = $value;
		}
	}

	/**
	 * @return int
	 */
	public function getRangeMinValue()
	{
		if ($this->_type->getId() != FilterType::INTEGER)
			return;

		return $this->_rangeMinValue;
	}

	/**
	 * @return int
	 */
	public function getCurrentMaxValue()
	{
		if ($this->_type->getId() != FilterType::INTEGER)
			return;

		return $this->_currentMaxValue;
	}

	/**
	 * @return int
	 */
	public function getCurrentMinValue()
	{
		if ($this->_type->getId() != FilterType::INTEGER)
			return;

		return $this->_currentMinValue;
	}

	public function applyCriteria(CDbCriteria $criteria)
	{
		if ($this->_type->getId() == FilterType::STRING)
		{
			$selectedValues = explode(";", $this->_value);
			foreach ($this->_options as $filterOption)
			{
				if (in_array($filterOption->value, $selectedValues))
				{
					$filterOption->isChecked = true;
				}
			}
		}

		if (!$this->getIsChecked())
			return;

		if ($this->_id == "manufacturer")
		{
			$selectedValues = array();
			foreach ($this->_options as $filterOption)
			{
				if ($filterOption->isChecked)
				{
					$selectedValues[] = $filterOption->value;
				}
			}

			$criteria->addInCondition("manufacturer_id", $selectedValues);
		}
		elseif ($this->_id == "price")
		{
			$criteria->addBetweenCondition("price", $this->_currentMinValue, $this->_currentMaxValue);
		}
		else
		{
			$subCriteria = new CDbCriteria();
			$subCriteria->join = 'LEFT OUTER JOIN {{filter_value_to_item}} `filtersValues` ON (`filtersValues`.`item_id`=`t`.`id`)';
			$subCriteria->params = array('filterId' . $this->_id => $this->_id);

			switch ($this->_type->getId())
			{
				case FilterType::INTEGER:
					$subCriteria->condition = 'filter_id = :filterId' . $this->_id . ' AND mixed_value BETWEEN :minValue' . $this->_id . ' AND :maxValue' . $this->_id;
					$subCriteria->params['minValue' . $this->_id] = $this->_currentMinValue;
					$subCriteria->params['maxValue' . $this->_id] = $this->_currentMaxValue;
					break;
				case FilterType::BOOLEAN:
					if ($this->_value == "1")
					{
						$subCriteria->condition = 'filter_id = :filterId' . $this->_id . ' AND mixed_value > 0';

					}
					elseif ($this->_value == "0")
					{
						$subCriteria->condition = 'filter_id = :filterId' . $this->_id . ' AND (mixed_value IS NULL OR mixed_value = 0)';
					}
					break;
				case FilterType::STRING:
					$selectedValues = array();
					foreach ($this->_options as $filterOption)
					{
						if ($filterOption->isChecked)
						{
							$selectedValues[] = "'" . $filterOption->value . "'";
						}
					}

					$subCriteria->condition = 'filter_id = :filterId' . $this->_id . ' AND mixed_value IN (' . join(",", $selectedValues) . ')';
					break;
			}
			$subCriteria->together = true;
			$criteria->mergeWith($subCriteria, false);
		}
	}

	public function getIsChecked()
	{
		switch ($this->_type->getId())
		{
			case FilterType::INTEGER:
				return !($this->_currentMinValue == $this->_rangeMinValue && $this->_currentMaxValue == $this->_rangeMaxValue);
			case FilterType::STRING:
				foreach ($this->_options as $option)
				{
					if ($option->getIsChecked())
						return true;
				}
				return false;
			case FilterType::BOOLEAN:
				return ($this->_value == "0" || $this->_value == "1");
		}
	}
}
<?

class FilterOption extends CComponent
{
    /**
     * @var string
     */
    private $_value;
    /**
     * @var string
     */
    private $_text;

    /**
     * @var bool
     */
    private $_isChecked;

    /**
     * @param string $value
     * @param string $text
     * @param int $itemsCount
     */
    public function __construct($value, $text, $checked = false)
    {
        $this->_value = $value;
        $this->_text = $text;
        $this->_isChecked =$checked;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->_text;
    }

    /**
     * @param boolean $isChecked
     */
    public function setIsChecked($isChecked)
    {
        $this->_isChecked = $isChecked;
    }

    /**
     * @return boolean
     */
    public function getIsChecked()
    {
        return $this->_isChecked;
    }
}
<?

class FilterOptionsCollection extends CComponent implements Iterator
{
    private $_items = array();
    private $_position = 0;

    /**
     * @param FilterOption $option
     * @return void
     */
    public function Add(FilterOption $option)
    {

        $this->_items[] = $option;
    }

    /**
     * @return FilterOption
     */
    public function current()
    {
        return $this->_items[$this->_position];
    }

    /**
     * @return void
     */
    public function next()
    {
        $this->_position++;
    }

    /**
     * @return string
     */
    public function key()
    {
        return (string)$this->_position;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return ($this->_position >= 0 && $this->_position < count($this->_items));
    }

    /**
     * @return void
     */
    public function rewind()
    {
        $this->_position = 0;
    }

    public function getLength()
    {
        return count($this->_items);
    }
}
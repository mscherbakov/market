<?

class FiltersCollection extends CComponent implements Iterator
{
	private $_items = array();
	private $_position = 0;

	/**
	 * @param Filter $filter
	 * @return void
	 */
	public function Add(FilterObject $filter)
	{
		$this->_items[] = $filter;
	}

	/**
	 * @return Filter
	 */
	public function current()
	{
		return $this->_items[$this->_position];
	}

	/**
	 * @return void
	 */
	public function next()
	{
		$this->_position++;
	}

	/**
	 * @return string
	 */
	public function key()
	{
		return (string)$this->_position;
	}

	/**
	 * @return bool
	 */
	public function valid()
	{
		return ($this->_position >= 0 && $this->_position < count($this->_items));
	}

	/**
	 * @return void
	 */
	public function rewind()
	{
		$this->_position = 0;
	}

	public function applyCriteria(CDbCriteria $criteria)
	{
		$subCriteria = new CDbCriteria();
		$nonEmptyItemsCount = 0;
		foreach ($this->_items as $item)
		{
			if ($item->getId() == 'manufacturer' || $item->getId() == 'price')
			{
				$item->applyCriteria($criteria);
			}
			else
			{
				$subCriteria2 = new CDbCriteria();
				$item->applyCriteria($subCriteria2);
				$subCriteria->mergeWith($subCriteria2, false);

				if ($item->isChecked)
				{
					$nonEmptyItemsCount++;
				}
			}
		}

		if ($nonEmptyItemsCount > 0)
		{
			$criteria->having = "count(filter_id) = " . $nonEmptyItemsCount;
			$criteria->group = 't.id';
		}
		$criteria->mergeWith($subCriteria);
	}

	/**
	 * @param array $actionParams
	 * @return void
	 */
	public function unsetParams(&$actionParams)
	{
		foreach ($this->_items as $item)
		{
			$item->unsetParams($actionParams);
		}
	}
}
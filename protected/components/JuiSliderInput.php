<?
Yii::import("zii.widgets.jui.CJuiSliderInput");

class JuiSliderInput extends CJuiSliderInput
{
    public $prefixMin = "от";
    public $prefixMax = "до";
    public $maxValue = 0;

    public function run()
    {
        list($name, $id) = $this->resolveNameID();

        $isRange = isset($this->options['range']) && $this->options['range'];

        if (isset($this->htmlOptions['id']))
            $id = $this->htmlOptions['id'];
        else
            $this->htmlOptions['id'] = $id;
        if (isset($this->htmlOptions['name']))
            $name = $this->htmlOptions['name'];

        if ($this->hasModel()) {
            $attribute = $this->attribute;
            if ($isRange) {
                $options = $this->htmlOptions;
                echo $this->prefixMin;
                echo CHtml::activeTextField($this->model, $this->attribute, $options);
                $options['id'] = $options['id'] . '_end';
                echo $this->prefixMax;
                echo CHtml::activeTextField($this->model, $this->maxAttribute, $options);
                $attrMax = $this->maxAttribute;
                $this->options['values'] = array($this->model->$attribute, $this->model->$attrMax);
            }
            else
            {
                echo CHtml::activeHiddenField($this->model, $this->attribute, $this->htmlOptions);
                $this->options['value'] = $this->model->$attribute;
            }
        }
        else
        {
            if ($isRange) {
                $options = $this->htmlOptions;
                echo CHtml::label($this->prefixMin, $options['id']);
                echo CHtml::textField($name."_minValue", ($this->options['min'] == $this->value && $this->options['max'] == $this->maxValue) ? "" : $this->value, $options);
                $options['id'] = $options['id'] . '_end';
                echo CHtml::label($this->prefixMax, $options['id']);
                echo CHtml::textField($name."_maxValue", ($this->options['min'] == $this->value && $this->options['max'] == $this->maxValue) ? "" :  $this->maxValue, $options);
                $this->options['values'] = array($this->value, $this->maxValue);
            }
            else
            {
                echo CHtml::hiddenField($name, $this->value, $this->htmlOptions);
                if ($this->value !== null)
                    $this->options['value'] = $this->value;
            }
        }


        $idHidden = $this->htmlOptions['id'];
        $nameHidden = $name;

        $this->htmlOptions['id'] = $idHidden . '_slider';
        $this->htmlOptions['name'] = $nameHidden . '_slider';

        echo CHtml::openTag($this->tagName, $this->htmlOptions);
        echo CHtml::closeTag($this->tagName);

        $this->options[$this->event] = $isRange ?
                "js:function(e,ui){ v=ui.values; if(v[0] == jQuery('#{$id}_slider').slider('option', 'min') && v[1] == jQuery('#{$id}_slider').slider('option', 'max')){jQuery('#{$idHidden}').val(''); jQuery('#{$idHidden}_end').val('');}else{ jQuery('#{$idHidden}').val(v[0]); jQuery('#{$idHidden}_end').val(v[1]); }}"
                :
                'js:function(event, ui) { jQuery(\'#' . $idHidden . '\').val(ui.value); }';

        $options = empty($this->options) ? '' : CJavaScript::encode($this->options);

        $js = "jQuery('#{$id}_slider').slider($options);\n$('#{$idHidden}, #{$idHidden}_end').change(function() {jQuery('#{$id}_slider').slider('values', [jQuery('#{$idHidden}').val(), jQuery('#{$idHidden}_end').val()]);});\n";
        Yii::app()->getClientScript()->registerScript(__CLASS__ . '#' . $id, $js);
    }
}
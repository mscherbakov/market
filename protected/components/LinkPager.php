<?

class LinkPager extends CLinkPager
{
	public function run()
	{
		echo "<span>Показано <span class='bold'>";
		echo $this->getPages()->getOffset() + 1;
		echo "</span> - <span class='bold'>";
		echo min($this->getPages()->getOffset() + $this->getPageSize(), $this->getItemCount());
		echo "</span> (всего <span class='bold'>";
		echo $this->getItemCount();
		echo "</span> позиций)</span>";

		parent::run();
	}
}

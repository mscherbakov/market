<?

class SpacesFilter extends CValidator
{
    /**
     * Validates a single attribute.
     * This method should be overridden by child classes.
     * @param CModel $object the data object being validated
     * @param string $attribute the name of the attribute to be validated.
     */
    protected function validateAttribute($object, $attribute)
    {
        $value = $object->$attribute;
        if (is_string($value))
        {
            $value = mbereg_replace("/\s+/", " ", $value);
            $value = trim($value);
            $object->$attribute = $value;
        }
    }
}

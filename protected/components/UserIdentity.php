<?php

class UserIdentity extends CUserIdentity
{
	private $_id;

	public function authenticate()
	{
		$criteria = new CDbCriteria();
		$criteria->addColumnCondition(array(
										   'market_id' => 1,
										   'login' => $this->username
									  ));
		$criteria->addCondition('password = PASSWORD(:password)');
		$criteria->params['password'] = $this->password;
		$user = MarketUser::model()->find($criteria);
		if (!$user)
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		else
		{
			$this->_id = $user->id;
			$this->username = $user->login;
			$this->errorCode = self::ERROR_NONE;
		}
		return $this->errorCode == self::ERROR_NONE;
	}

	public function trustAuthenticate()
	{
		$criteria = new CDbCriteria();
		$criteria->addColumnCondition(array(
										   'market_id' => 1,
										   'login' => $this->username,
										   'password' => $this->password
									  ));
		$user = MarketUser::model()->find($criteria);
		if (!$user)
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		else
		{
			$this->_id = $user->id;
			$this->username = $user->login;
			$this->errorCode = self::ERROR_NONE;
		}
		return $this->errorCode == self::ERROR_NONE;
	}

	public function getId()
	{
		return $this->_id;
	}
}
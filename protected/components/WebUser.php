<?

class WebUser extends CWebUser
{
    private $_model;
    public $cookieDomain;

    public function getModel()
    {
        if ($this->getIsGuest())
        {
            return null;
        }

        if (!$this->_model)
        {
            $this->_model = MarketUser::model()->findByPk($this->getId());
        }

        return $this->_model;
    }

    public function getWarehouse()
    {
        if ($this->getIsGuest())
            return;

        $warehouseId = (int)$this->getState('warehouseId');
        if (!$warehouseId || !in_array($warehouseId, array($this->getModel()->warehouse_id, Yii::app()->params['defaultWarehouseId'])))
            $warehouseId = (int)$this->getModel()->warehouse_id;

        $warehouse = MarketWarehouse::model()->findByAttributes(array(
                                                                     'market_id' => Yii::app()->params['marketId'],
                                                                     'market_warehouse_id' => $warehouseId,
                                                                ));

        if ($warehouse)
            return $warehouse;
        else
            return MarketWarehouse::model()->findByAttributes(array(
                                                                   'market_id' => Yii::app()->params['marketId'],
                                                                   'market_warehouse_id' => Yii::app()->params['defaultWarehouseId'],
                                                              ));
    }

    public function getWarehouseListData()
    {
        $warehouseIds = array(Yii::app()->params['defaultWarehouseId']);
        if ($this->getModel()->warehouse_id)
            $warehouseIds[] = $this->getModel()->warehouse_id;

        $criteria = new CDbCriteria();
        $criteria->addColumnCondition(array('market_id' => Yii::app()->params['marketId']));
        $criteria->addInCondition('market_warehouse_id', $warehouseIds);

        return CHtml::listData(MarketWarehouse::model()->findAll($criteria), 'market_warehouse_id', 'name');
    }

    public function login($identity, $duration = 0)
    {
        $id = $identity->getId();
        $states = $identity->getPersistentStates();
        if ($this->beforeLogin($id, $states, false))
        {
            $this->changeIdentity($id, $identity->getName(), $states);

            $cookieExpire = time() + $duration;

            $PHP_AUTH_LANG_cookie = new CHttpCookie("PHP_AUTH_LANG", 'Russian');
            $PHP_AUTH_LANG_cookie->expire = $cookieExpire;
            $PHP_AUTH_LANG_cookie->domain = $this->cookieDomain;
            Yii::app()->getRequest()->getCookies()->add($PHP_AUTH_LANG_cookie->name, $PHP_AUTH_LANG_cookie);

            $PHP_AUTH_USER_cookie = new CHttpCookie("PHP_AUTH_USER", $identity->username);
            $PHP_AUTH_USER_cookie->expire = $cookieExpire;
            $PHP_AUTH_USER_cookie->domain = $this->cookieDomain;
            Yii::app()->getRequest()->getCookies()->add($PHP_AUTH_USER_cookie->name, $PHP_AUTH_USER_cookie);

            $PHP_AUTH_PW_cookie = new CHttpCookie("PHP_AUTH_PW", $this->getModel()->password);
            $PHP_AUTH_PW_cookie->expire = $cookieExpire;
            $PHP_AUTH_PW_cookie->domain = $this->cookieDomain;
            Yii::app()->getRequest()->getCookies()->add($PHP_AUTH_PW_cookie->name, $PHP_AUTH_PW_cookie);

            if ($duration > 0)
            {
                if ($this->allowAutoLogin)
                    $this->saveToCookie($duration);
                else
                    throw new CException(Yii::t('yii', '{class}.allowAutoLogin must be set true in order to use cookie-based authentication.',
                        array('{class}' => get_class($this))));
            }

            $this->afterLogin(false);
        }
    }

    public function logout($destroySession = true)
    {
        if ($this->beforeLogout())
        {
            if ($this->allowAutoLogin)
            {
                $PHP_AUTH_LANG_cookie = new CHttpCookie("PHP_AUTH_LANG", null);
                $PHP_AUTH_LANG_cookie->domain = $this->cookieDomain;
                Yii::app()->getRequest()->getCookies()->add($PHP_AUTH_LANG_cookie->name, $PHP_AUTH_LANG_cookie);

                $PHP_AUTH_USER_cookie = new CHttpCookie("PHP_AUTH_USER", null);
                $PHP_AUTH_USER_cookie->domain = $this->cookieDomain;
                Yii::app()->getRequest()->getCookies()->add($PHP_AUTH_USER_cookie->name, $PHP_AUTH_USER_cookie);

                $PHP_AUTH_PW_cookie = new CHttpCookie("PHP_AUTH_PW", null);
                $PHP_AUTH_PW_cookie->domain = $this->cookieDomain;
                Yii::app()->getRequest()->getCookies()->add($PHP_AUTH_PW_cookie->name, $PHP_AUTH_PW_cookie);

                if ($this->identityCookie !== null)
                {
                    $cookie = $this->createIdentityCookie($this->getStateKeyPrefix());
                    $cookie->value = null;
                    $cookie->expire = 0;
                    Yii::app()->getRequest()->getCookies()->add($cookie->name, $cookie);
                }
            }
            if ($destroySession)
                Yii::app()->getSession()->destroy();
            else
                $this->clearStates();
            $this->afterLogout();
        }
    }
}

<?php

class Widget extends CWidget
{

    public function getViewPath($checkTheme = false)
    {
        $class = new ReflectionClass(get_class($this));
        $name = $class->getName();
        $pos = strrpos($name, 'Widget');
        if ($pos !== FALSE)
        {
            $name = substr_replace($name, '', $pos);
        }
        return dirname($class->getFileName()) . '/' . $name;
    }

}

<?php

return CMap::mergeArray(
	CMap::mergeArray(
		require(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'main.php'),
		require(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'productionDB.php')
	),
	array(
		 'language' => 'en',
	)
);
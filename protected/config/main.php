<?

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Market',
    'language' => 'ru',

    'import' => array(
        'application.models.*',
        'application.widgets.*',
        'application.helpers.*',
        'application.components.*',
    ),

    'components' => array(
        'user' => array(
            'class' => 'application.components.WebUser',
            'allowAutoLogin' => true,
        ),

        'db' => array(
            'emulatePrepare' => true,
            'charset' => 'utf8',
            'tablePrefix' => '',
        ),

        'b2bdb' => array(
            'class' => 'system.db.CDbConnection',
            'emulatePrepare' => true,
            'charset' => 'utf8',
            'tablePrefix' => '',
        ),

        'format' => array(
            'datetimeFormat' => 'd.m.Y H:i:s',
            'numberFormat' => array(
                'decimals' => 2,
                'decimalSeparator' => '.',
                'thousandSeparator' => ' ',
            ),
        ),

        'image' => array(
            'class' => 'application.extensions.image.CImageComponent',
            'driver' => 'GD',
        ),

        'cache' => array(
            'class' => 'system.caching.CDummyCache',
        ),

        'clientScript' => array(
            'packages' => array(
                'item' => array(
                    'baseUrl' => 'js',
                    'js' => array('item.js'),
                    'depends' => array('jquery'),
                ),
                'filter' => array(
                    'baseUrl' => 'js',
                    'js' => array('filter.js'),
                    'depends' => array('jquery'),
                ),
            ),
        ),
    ),

    'params' => array(
        'categoriesPerRow' => 7,
        'defaultItemsPerPage' => 10,
        'latestItemsPerRow' => 5,
        "imagesPath" => "images" . DIRECTORY_SEPARATOR . "items",
        "imageFilenameLength" => 6,
        "imagePreviewsPath" => "images" . DIRECTORY_SEPARATOR . "itemPreviews",
        "imagePreviewFilenameLength" => 6,
        'cacheDuration' => 3600,
        'b2bReplicationQueryLimit' => 5000,
        'marketId' => 1,
        'defaultWarehouseId' => 11,
    ),
);
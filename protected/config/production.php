<?php

return CMap::mergeArray(
	CMap::mergeArray(
		require(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'main.php'),
		require(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'productionDB.php')
	),
	array(
		 'defaultController' => 'category',

		 'components' => array(
             'user' => array(
                 'cookieDomain' => '',
             ),

			 'cache' => array(
//				 'class' => 'system.caching.CApcCache',
			 ),
		 ),

		 'params' => array(
			 'b2bdomen' => '',
		 ),
	)
);

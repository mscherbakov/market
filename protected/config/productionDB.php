<?php

return array(
	'components' => array(
		'db' => array(
			'connectionString' => 'mysql:host=localhost;dbname=newmarket',
			'username' => 'market',
			'password' => '',
			'schemaCachingDuration' => 3600,
		),

		'b2bdb' => array(
			'connectionString' => 'mysql:host=localhost;dbname=b2b',
			'username' => 'b2b',
			'password' => '',
			'schemaCachingDuration' => 3600,
		),
	),
);

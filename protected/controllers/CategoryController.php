<?

class CategoryController extends Controller
{
    public function actionIndex()
    {
        $models = Category::model()->root()->visible()->with('children:visible')->findAll();
        $this->render('index', array('models' => $models));
    }

    public function actionView($id)
    {
        $this->render('view', array('model' => $this->loadModel($id)));
    }

    public function loadModel($id)
    {
        $model = Category::model()->visible()->with('children:visible')->findByPk($id);
        if ($model === null)
            throw new CHttpException(404);
        return $model;
    }
}

?>
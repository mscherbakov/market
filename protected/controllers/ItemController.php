<?

class ItemController extends Controller
{
    public function actionView($id)
    {
    	$model = $this->loadModel($id);
    	$similars = $model->similarAuto ();
    	if (!is_array ($similars) OR sizeof ($similars) <= 0)
    	{
    		$similars = $model->similarFilter ();
    	}
        $this->render('view', array(
        	'model' 		=> $model, 
        	'similars'	=> $similars,
        ));
    }

    public function loadModel($id)
    {
        $model = Item::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404);
        return $model;
    }
}
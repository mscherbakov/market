<?php

class SearchController extends Controller
{
	public function actionIndex($categoryId = null)
	{
		$keyword = Yii::app()->request->getQuery('keyword');
		$category = null;

		if ($categoryId)
		{
			$category = Category::model()->findByPk($categoryId);
		}

		$this->render("index", array(
									'keyword' => $keyword,
									'category' => $category,
							   ));
	}
}
<?

class SiteController extends Controller
{
    public function accessRules()
    {
        return array(
            array('allow',
                  'users' => array('*'),
            ),
            array('deny'),
        );
    }

    public function actionLogin()
    {
        if (!Yii::app()->user->getIsGuest())
            $this->redirect(Yii::app()->homeUrl);

        $model = new LoginForm();

        if ($attributes = Yii::app()->request->getPost(get_class($model)))
        {
            $model->attributes = $attributes;

            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }

        $this->render('login', array('model' => $model));
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
}

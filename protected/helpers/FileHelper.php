<?

class FileHelper extends CFileHelper
{
	/**
	 * @static
	 * @param string $directoryPath
	 * @param string $fileExtension
	 * @param int $filenameLength
	 * @return string
	 */
	public static function getUniqueFilename($directoryPath, $fileExtension, $filenameLength)
	{
		if ($filenameLength < 1)
		{
			$filenameLength = 1;
		}

		do
		{
			$filename = RandomHelper::getRandString($filenameLength) . "." . $fileExtension;
		} while (is_file($directoryPath . DIRECTORY_SEPARATOR . $filename));

		return $filename;
	}

	/**
	 * @static
	 * @param string $filepath
	 * @return string
	 */
	public static function getBasename($filepath)
	{
		$path_parts = pathinfo($filepath);
		return $path_parts['basename'];
	}

	/**
	 * @static
	 * @param $filepath
	 * @return string
	 */
	public static function getFilename($filepath)
	{
		$path_parts = pathinfo($filepath);
		return $path_parts['filename'];
	}
}

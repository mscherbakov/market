<?

class RandomHelper
{
	/**
	 * @static
	 * @param int $length
	 * @return string
	 */
	public static function getRandString($length)
	{
		$chars = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
		$charsCount = count($chars);
		$result = "";
		for ($i = 0; $i < $length; $i++)
		{
			$result .= $chars[rand(0, $charsCount - 1)];
		}

		return $result;
	}
}

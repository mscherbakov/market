<?

/**
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property boolean $is_hidden
 * @property integer $order
 * @property integer $author_id
 * @property integer $editor_id
 * @property integer $create_time
 * @property integer $update_time
 *
 * @property User $editor
 * @property Category $parent
 * @property Category[] $children
 * @property integer $childrenCount
 * @property User $author
 * @property Item[] $items
 * @property integer $itemCount
 * @property Filter[] $filters
 * @property ItemPropertyGroup[] $propertyGroups
 */
class Category extends ActiveRecord
{
    private static $_models;
    private $_isDeleteChildren = false;
    private $_isDeleteItems = false;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{category}}';
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ),
            'AuthorBehavior' => array(
                'class' => 'application.components.AuthorBehavior',
                'setEditorOnCreate' => true,
            ),
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
            ),
        );
    }

    public function rules()
    {
        return array(
            array('parent_id', 'exist', 'className' => 'Category', 'attributeName' => 'id'),
            array('parent_id', 'parentValidator', 'skipOnError' => true),
            array('name', 'required'),
            array('name', 'length', 'max' => 255),
            array('name', 'SpacesFilter'),
            array('parent_id+name', 'ext.uniqueMultiColumnValidator'),
            array('is_hidden', 'boolean'),
            array('order', 'numerical', 'integerOnly' => true),
            array('order', 'default', 'value' => 0),

            array('id, parent_id, name, author_id, editor_id', 'safe', 'on' => 'search'),
        );
    }

    public function parentValidator($attribute, $params)
    {
        $model = Category::model()->findByPk($this->{$attribute});
        if ($model)
        {
            if ($model->itemCount > 0)
                $this->addError($attribute, 'Категория не может быть родительской, т.к. включает в себя товары');
            elseif (!$this->getIsNewRecord())
            {
                while ($model)
                {
                    if ($this->id == $model->id)
                    {
                        $this->addError($attribute, 'Категория не может быть родительской, т.к. является подкатегорией текущей категории');
                        return;
                    }

                    $model = $model->parent;
                }
            }
        }
    }

    public function relations()
    {
        return array(
            'editor' => array(self::BELONGS_TO, 'User', 'editor_id'),
            'parent' => array(self::BELONGS_TO, 'Category', 'parent_id'),
            'children' => array(self::HAS_MANY, 'Category', 'parent_id'),
            'childrenCount' => array(self::STAT, 'Category', 'parent_id'),
            'author' => array(self::BELONGS_TO, 'User', 'author_id'),
            'items' => array(self::HAS_MANY, 'Item', 'category_id'),
            'itemCount' => array(self::STAT, 'Item', 'category_id'),
            'filters' => array(self::HAS_MANY, 'Filter', 'category_id'),
            'propertyGroups' => array(self::HAS_MANY, 'ItemPropertyGroup', 'category_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '№',
            'parent_id' => 'Родительская категория',
            'name' => 'Название',
            'is_hidden' => 'Скрыт',
            'order' => 'Порядок вывода',
            'author_id' => 'Автор',
            'editor_id' => 'Редактор',
            'create_time' => 'Создано',
            'update_time' => 'Редактировано',
        );
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);

        return array(
            'order' => $alias . '.order, ' . $alias . '.name',
        );
    }

    public function scopes()
    {
        return array(
            'root' => array(
                'condition' => $this->getTableAlias() . '.parent_id is null',
            ),
            'visible' => array(
                'condition' => $this->getTableAlias() . '.is_hidden = 0',
            ),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        if ($this->parent_id == '0')
            $criteria->addCondition('parent_id is null');
        else
            $criteria->compare('parent_id', $this->parent_id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('is_hidden', $this->is_hidden);
        $criteria->compare('author_id', $this->author_id);
        $criteria->compare('editor_id', $this->editor_id);

        return new CActiveDataProvider($this, array('criteria' => $criteria));
    }

    protected function beforeDelete()
    {
        $this->beginTransaction();

        if ($this->_isDeleteChildren)
        {
            foreach ($this->children as $child)
            {
                if (!$child->delete($this->_isDeleteChildren, $this->_isDeleteItems))
                    return false;
            }
        }
        else
        {
            $criteria = new CDbCriteria();
            $criteria->addColumnCondition(array('parent_id' => $this->id));
            Category::model()->updateAll(array('parent_id' => $this->parent_id), $criteria);
        }

        if ($this->_isDeleteItems)
        {
            foreach ($this->items as $item)
            {
                if (!$item->delete())
                    return false;
            }
        }
        else
        {
            $criteria = new CDbCriteria();
            $criteria->addColumnCondition(array('category_id' => $this->id));
            Item::model()->updateAll(array('category_id' => null), $criteria);
        }

        foreach ($this->filters as $filter)
        {
            if (!$filter->delete())
                return false;
        }

        return parent::beforeDelete();
    }

    public function delete($isDeleteChildren = false, $isDeleteItems = false)
    {
        $this->_isDeleteChildren = $isDeleteChildren;
        $this->_isDeleteItems = $isDeleteItems;
        return parent::delete();
    }

    /**
     * @static
     * @return Category[]
     */
    public static function getModels()
    {
        if (!self::$_models)
        {
            $criteria = new CDbCriteria();
            $criteria->index = 'id';
            $criteria->with = array('itemCount');

            self::$_models = self::model()->findAll($criteria);

            foreach (self::$_models as $model)
            {
                $model->parent = null;
                $model->children = array();
            }

            foreach (self::$_models as $model)
            {
                if ($model->parent_id)
                {
                    $tmp = self::$_models[$model->parent_id]->children;
                    $tmp[] = $model;
                    self::$_models[$model->parent_id]->children = $tmp;

                    $model->parent = self::$_models[$model->parent_id];
                }
            }
        }

        return self::$_models;
    }

    /**
     * @static
     * @param Category $model
     * @param array $result
     * @param bool $isOnlyWithoutChildrenSelectable
     * @param bool $isOnlyWithoutItemsSelectable
     * @param bool $isShowTotalItemCount
     * @param array $notSelectableModelIds
     * @param int $deep
     */
    private static function getRecursiveListData(Category $model, &$result, $isOnlyWithoutChildrenSelectable, $isOnlyWithoutItemsSelectable, $isShowTotalItemCount, $notSelectableModelIds = array(), $deep = 0)
    {
        $repeatString = "....";

        $modelString = $model->name;
        if (count($model->children) > 0 && $model->itemCount > 0)
            $modelString = '!!!!' . $modelString;
        if ($isShowTotalItemCount)
            $modelString .= ' (' . Yii::t('app', '{n} товар|{n} товара|{n} товаров|{n} товара', $model->getTotalItemCount()) . ')';
        $modelString = str_repeat($repeatString, $deep) . $modelString;

        if (in_array($model->id, $notSelectableModelIds))
        {
            $isOnlyWithoutChildrenSelectable = true;
            $isOnlyWithoutItemsSelectable = true;
        }

        if (($isOnlyWithoutChildrenSelectable && count($model->children) > 0) || ($isOnlyWithoutItemsSelectable && $model->itemCount > 0))
            $result[$modelString] = array();
        else
            $result[$model->id] = $modelString;

        foreach ($model->children as $child)
            self::getRecursiveListData($child, $result, $isOnlyWithoutChildrenSelectable, $isOnlyWithoutItemsSelectable, $isShowTotalItemCount, $notSelectableModelIds, $deep + 1);
    }

    /**
     * @static
     * @param bool $isOnlyWithoutChildrenSelectable
     * @param bool $isOnlyWithoutItemsSelectable
     * @param bool $isShowTotalItemCount
     * @param array $notSelectableModelIds
     * @return array
     */
    public static function getListData($isOnlyWithoutChildrenSelectable = false, $isOnlyWithoutItemsSelectable = false, $isShowTotalItemCount = false, $notSelectableModelIds = array())
    {
        $result = array();

        foreach (self::getModels() as $model)
            if (!$model->parent_id)
                self::getRecursiveListData($model, $result, $isOnlyWithoutChildrenSelectable, $isOnlyWithoutItemsSelectable, $isShowTotalItemCount, $notSelectableModelIds);

        return $result;
    }

    /**
     * @param bool $isShowTotalItemCount
     * @return array
     */
    public function getNewParentListData($isShowTotalItemCount = false)
    {
        if ($this->getIsNewRecord())
            $notSelectableModelIds = array();
        else
            $notSelectableModelIds = array($this->id);

        return $this->getListData(false, true, $isShowTotalItemCount, $notSelectableModelIds);
    }

    /**
     * @return int
     */
    public function getTotalItemCount()
    {
        $result = $this->itemCount;

        foreach ($this->children as $child)
            $result += $child->getTotalItemCount();

        return $result;
    }

    /**
     * @param string $keyword
     * @return array
     */
    public function findAllByItemKeyword($keyword)
    {
        $command = Yii::app()->db->createCommand()
            ->select('category.*')
            ->from('{{category}} category')
            ->leftJoin('{{item}} item', 'item.category_id=category.id')
            ->where('item.name LIKE :keyword AND category.is_hidden=0', array('keyword' => '%' . $keyword . '%'))
            ->order('category.name')
            ->group('category.id');
        $queryResult = $command->queryAll();

        return $this->populateRecords($queryResult, false);
    }
}
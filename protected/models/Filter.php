<?

/**
 * @property integer $id
 * @property integer $category_id
 * @property integer $type_id
 * @property integer $integer_accuracy
 * @property string $integer_unit
 * @property string $name
 * @property integer $is_extended
 * @property integer $order
 * @property integer $author_id
 * @property integer $editor_id
 * @property integer $create_time
 * @property integer $update_time
 *
 * @property Category $category
 * @property FilterType $type
 * @property User $author
 * @property User $editor
 * @property FilterValue[] $values
 * @property FilterValueToItem[] $valueToItems
 */
class Filter extends ActiveRecord
{
    private $_minValue;
    private $_maxValue;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{filter}}';
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ),
            'AuthorBehavior' => array(
                'class' => 'application.components.AuthorBehavior',
                'setEditorOnCreate' => true,
            ),
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
            ),
        );
    }

    public function rules()
    {
        return array(
            array('category_id, type_id, name', 'required'),
            array('category_id', 'exist', 'className' => 'Category', 'attributeName' => 'id'),
            array('category_id', 'CategoryValidator', 'skipOnError' => true),
            array('type_id', 'exist', 'className' => 'FilterType', 'attributeName' => 'id'),
            array('integer_unit, name', 'length', 'max' => 255),
            array('integer_unit, name', 'SpacesFilter'),
            array('category_id+name', 'ext.uniqueMultiColumnValidator'),
            array('integer_accuracy, is_extended, order', 'default', 'value' => 0),
            array('integer_accuracy', 'in', 'range' => array(0, 1, 2, 3)),
            array('is_extended', 'boolean'),
            array('order', 'numerical', 'integerOnly' => true),

            array('id, category_id, type_id, name, is_extended, author_id, editor_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
            'type' => array(self::BELONGS_TO, 'FilterType', 'type_id'),
            'author' => array(self::BELONGS_TO, 'User', 'author_id'),
            'editor' => array(self::BELONGS_TO, 'User', 'editor_id'),
            'values' => array(self::HAS_MANY, 'FilterValue', 'filter_id', 'index' => 'id'),
            'valueToItems' => array(self::HAS_MANY, 'FilterValueToItem', 'filter_id', 'index' => 'item_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '№',
            'category_id' => 'Категория',
            'type_id' => 'Тип',
            'integer_accuracy' => 'Символов после точки',
            'integer_unit' => 'Единица измерения',
            'name' => 'Название',
            'is_extended' => 'Расширенный',
            'order' => 'Порядок вывода',
            'author_id' => 'Автор',
            'editor_id' => 'Редактор',
            'create_time' => 'Создано',
            'update_time' => 'Редактировано',
        );
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);

        return array(
            'order' => $alias . '.is_extended, ' . $alias . '.order, ' . $alias . '.name',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('category_id', $this->category_id);
        $criteria->compare('type_id', $this->type_id);
        $criteria->compare('integer_accuracy', $this->integer_accuracy);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('is_extended', $this->is_extended);
        $criteria->compare('order', $this->order);
        $criteria->compare('author_id', $this->author_id);
        $criteria->compare('editor_id', $this->editor_id);
        $criteria->compare('create_time', $this->create_time);
        $criteria->compare('update_time', $this->update_time);

        return new CActiveDataProvider($this, array('criteria' => $criteria));
    }

    protected function beforeDelete()
    {
        $this->beginTransaction();

        $criteria = new CDbCriteria();
        $criteria->addColumnCondition(array('filter_id' => $this->id));
        FilterValueToItem::model()->deleteAll($criteria);

        foreach ($this->values as $value)
        {
            if (!$value->delete())
                return false;
        }

        return parent::beforeDelete();
    }

    public function getValue()
    {
        $result = ($this->type_id == FilterType::TYPE_STRING ? array() : null);

        $value = Yii::app()->request->getQuery($this->id);

        if ($value)
        {
            if ($this->type_id == FilterType::TYPE_STRING)
            {
                $tmp = explode(';', $value);

                foreach ($tmp as $value)
                    if (isset($this->values[$value]))
                        $result[] = $value;
            }
            else
                $result = $value;
        }

        return $result;
    }

    public function getCurrentMinValue()
    {
        if ($this->type_id != FilterType::TYPE_INTEGER)
            return;

        if ($value = (int)Yii::app()->request->getQuery($this->id . '_minValue'))
            return $value;
        else
            return $this->getMinValue();
    }

    public function getCurrentMaxValue()
    {
        if ($this->type_id != FilterType::TYPE_INTEGER)
            return;

        if ($value = (int)Yii::app()->request->getQuery($this->id . '_maxValue'))
            return $value;
        else
            return $this->getMaxValue();
    }

    private function getMinMaxValue()
    {
        if ($this->type_id != FilterType::TYPE_INTEGER)
            return;

        $command = Yii::app()->db->createCommand()
            ->select('min(integer_value) as `minValue`, max(integer_value) as `maxValue`')
            ->from('{{filter_value_to_item}}')
            ->where('filter_id=:filterId AND integer_value > 0', array('filterId' => $this->id));

        $queryResult = $command->queryRow();

        $this->_minValue = (int)$queryResult['minValue'];
        $this->_maxValue = (int)$queryResult['maxValue'];
    }

    public function getMinValue()
    {
        if ($this->type_id != FilterType::TYPE_INTEGER)
            return;

        if (!$this->_minValue)
        {
            $this->getMinMaxValue();
        }

        return $this->_minValue;
    }

    public function setMinValue($value)
    {
        $this->_minValue = $value;
    }

    public function getMaxValue()
    {
        if ($this->type_id != FilterType::TYPE_INTEGER)
            return;

        if (!$this->_maxValue)
        {
            $this->getMinMaxValue();
        }

        return $this->_maxValue;
    }

    public function setMaxValue($value)
    {
        $this->_maxValue = $value;
    }

    public function getIsChecked()
    {
        switch ($this->type_id)
        {
            case FilterType::TYPE_STRING:
                return count($this->getValue()) > 0;
            case FilterType::TYPE_INTEGER:
                $result = Yii::app()->request->getQuery($this->id . '_minValue') || Yii::app()->request->getQuery($this->id . '_maxValue');
                if ($result)
                    $result = ($this->getMinValue() != $this->getCurrentMinValue()) || ($this->getMaxValue() != $this->getCurrentMaxValue());
                return $result;
            case FilterType::TYPE_BOOLEAN:
                return $this->getValue() == '0' || $this->getValue() == '1';
        }
    }

    public function applyCriteria(CDbCriteria $criteria)
    {
        if (!$this->getIsChecked())
            return false;

        if ($this->type_id == FilterType::TYPE_STRING)
        {
            $criteria->addInCondition($this->id == 'manufacturer' ? 't.manufacturer_id' : 'filter_value_id', $this->getValue());
        }
        elseif ($this->type_id == FilterType::TYPE_INTEGER)
        {
            $criteria->addBetweenCondition($this->id == 'price' ? 'price' : 'integer_value', $this->getCurrentMinValue(), $this->getCurrentMaxValue());
        }
        elseif ($this->type_id == FilterType::TYPE_BOOLEAN)
        {
            if ($this->getValue() == '1')
                $criteria->addCondition('boolean_value = 1');
            else
                $criteria->addCondition('boolean_value IS NULL OR boolean_value = 0');
        }

        if (!in_array($this->id, array('manufacturer', 'price')))
        {
            $criteria->together = true;
            $criteria->join = 'LEFT OUTER JOIN {{filter_value_to_item}} `filterValues` ON (`filterValues`.`item_id`=`t`.`id`)';
            $criteria->compare('filter_id', $this->id);
        }

        return true;
    }
}
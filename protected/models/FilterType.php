<?

/**
 * @property integer $id
 * @property string $name
 *
 * @property Filter[] $filters
 */
class FilterType extends CActiveRecord
{
    const TYPE_STRING = 1;
    const TYPE_INTEGER = 2;
    const TYPE_BOOLEAN = 3;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{filter_type}}';
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name', 'length', 'max' => 255),

            array('id, name', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'filters' => array(self::HAS_MANY, 'Filter', 'type_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider($this, array('criteria' => $criteria));
    }

    public static function getListData()
    {
        return CHtml::listData(self::model()->findAll(), 'id', 'name');
    }
}
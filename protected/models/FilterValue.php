<?

/**
 * @property integer $id
 * @property integer $filter_id
 * @property string $value
 * @property integer $order
 * @property integer $author_id
 * @property integer $editor_id
 * @property integer $create_time
 * @property integer $update_time
 *
 * @property Filter $filter
 * @property User $author
 * @property User $editor
 * @property FilterValueToItem[] $valueToItems
 */
class FilterValue extends ActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{filter_value}}';
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ),
            'AuthorBehavior' => array(
                'class' => 'application.components.AuthorBehavior',
                'setEditorOnCreate' => true,
            ),
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
            ),
        );
    }

    public function rules()
    {
        return array(
            array('filter_id, value', 'required'),
            array('filter_id', 'exist', 'className' => 'Filter', 'attributeName' => 'id'),
            array('value', 'length', 'max' => 255),
            array('value', 'SpacesFilter'),
            array('filter_id+value', 'ext.uniqueMultiColumnValidator'),
            array('order', 'default', 'value' => 0),
            array('order', 'numerical', 'integerOnly' => true),

            array('id, filter_id, value, author_id, editor_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'filter' => array(self::BELONGS_TO, 'Filter', 'filter_id'),
            'author' => array(self::BELONGS_TO, 'User', 'author_id'),
            'editor' => array(self::BELONGS_TO, 'User', 'editor_id'),
            'valueToItems' => array(self::HAS_MANY, 'FilterValueToItem', 'filter_value_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '№',
            'filter_id' => 'Фильтр',
            'value' => 'Значение',
            'order' => 'Порядок вывода',
            'author_id' => 'Автор',
            'editor_id' => 'Редактор',
            'create_time' => 'Создано',
            'update_time' => 'Редактировано',
        );
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);

        return array(
            'order' => $alias . '.order, ' . $alias . '.value',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('filter_id', $this->filter_id);
        $criteria->compare('value', $this->value, true);
        $criteria->compare('author_id', $this->author_id);
        $criteria->compare('editor_id', $this->editor_id);
        $criteria->compare('create_time', $this->create_time);
        $criteria->compare('update_time', $this->update_time);

        return new CActiveDataProvider($this, array('criteria' => $criteria));
    }

    protected function beforeDelete()
    {
        $this->beginTransaction();

        $criteria = new CDbCriteria();
        $criteria->addColumnCondition(array('filter_value_id' => $this->id));
        FilterValueToItem::model()->deleteAll($criteria);

        return parent::beforeDelete();
    }
}
<?

/**
 * @property integer $id
 * @property integer $filter_id
 * @property integer $item_id
 * @property integer $filter_value_id
 * @property string $integer_value
 * @property integer $boolean_value
 * @property integer $author_id
 * @property integer $editor_id
 * @property integer $create_time
 * @property integer $update_time
 *
 * @property Filter $filter
 * @property FilterValue $filterValue
 * @property Item $item
 * @property User $author
 * @property User $editor
 */
class FilterValueToItem extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{filter_value_to_item}}';
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ),
            'AuthorBehavior' => array(
                'class' => 'application.components.AuthorBehavior',
                'setEditorOnCreate' => true,
            ),
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
            ),
        );
    }

    public function rules()
    {
        return array(
            array('filter_id, item_id', 'required'),
            array('filter_id', 'exist', 'className' => 'Filter', 'attributeName' => 'id'),
            array('item_id', 'exist', 'className' => 'Item', 'attributeName' => 'id'),
            array('filter_value_id', 'exist', 'className' => 'FilterValue', 'attributeName' => 'id'),
            array('integer_value', 'numerical'),
            array('boolean_value', 'boolean'),

            array('id, filter_id, item_id, author_id, editor_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'filter' => array(self::BELONGS_TO, 'Filter', 'filter_id'),
            'filterValue' => array(self::BELONGS_TO, 'FilterValue', 'filter_value_id'),
            'item' => array(self::BELONGS_TO, 'Item', 'item_id'),
            'author' => array(self::BELONGS_TO, 'User', 'author_id'),
            'editor' => array(self::BELONGS_TO, 'User', 'editor_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'filter_id' => 'Filter',
            'item_id' => 'Item',
            'filter_value_id' => 'Filter Value',
            'integer_value' => 'Integer Value',
            'boolean_value' => 'Boolean Value',
            'author_id' => 'Author',
            'editor_id' => 'Editor',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('filter_id', $this->filter_id);
        $criteria->compare('item_id', $this->item_id);
        $criteria->compare('filter_value_id', $this->filter_value_id);
        $criteria->compare('integer_value', $this->integer_value, true);
        $criteria->compare('boolean_value', $this->boolean_value);
        $criteria->compare('author_id', $this->author_id);
        $criteria->compare('editor_id', $this->editor_id);
        $criteria->compare('create_time', $this->create_time);
        $criteria->compare('update_time', $this->update_time);

        return new CActiveDataProvider($this, array('criteria' => $criteria));
    }

    public function getValue()
    {
        switch ($this->filter->type_id)
        {
            case FilterType::TYPE_STRING:
                return $this->filter_value_id;
            case FilterType::TYPE_INTEGER:
                return $this->integer_value;
            case FilterType::TYPE_BOOLEAN:
                return $this->boolean_value;
        }
    }

    public function setValue($value)
    {
        switch ($this->filter->type_id)
        {
            case FilterType::TYPE_STRING:
                $this->filter_value_id = $value;
                break;
            case FilterType::TYPE_INTEGER:
                $this->integer_value = $value;
                break;
            case FilterType::TYPE_BOOLEAN:
                $this->boolean_value = $value;
                break;
        }
    }
}
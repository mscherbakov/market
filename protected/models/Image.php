<?

/**
 * @property integer $id
 * @property integer $width
 * @property integer $height
 * @property string $filename
 * @property integer $filesize
 * @property string $md5
 * @property integer $author_id
 * @property integer $create_time
 *
 * @property User $author
 * @property ImagePreview[] $previews
 * @property Item[] $items
 */
class Image extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{image}}';
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'updateAttribute' => null,
            ),
            'AuthorBehavior' => array(
                'class' => 'application.components.AuthorBehavior',
                'editorAttribute' => null,
            ),
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
            ),
        );
    }

    public function rules()
    {
        return array(
            array('width, height, filename, filesize, md5', 'required'),
            array('width, height, filesize', 'numerical', 'integerOnly' => true),
            array('filename', 'length', 'max' => 255),
            array('filename', 'unique'),
            array('md5', 'length', 'max' => 32),
            array('filesize+md5', 'ext.uniqueMultiColumnValidator'),

            array('id, width, height, filename, filesize, md5, author_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'author' => array(self::BELONGS_TO, 'User', 'author_id'),
            'previews' => array(self::HAS_MANY, 'ImagePreview', 'image_id'),
            'items' => array(self::MANY_MANY, 'Item', 'image_to_item(image_id, item_id)'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '№',
            'width' => 'Ширина',
            'height' => 'Высота',
            'filename' => 'Filename',
            'filesize' => 'Размер (в байтах)',
            'md5' => 'Md5',
            'author_id' => 'Автор',
            'create_time' => 'Создано',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('width', $this->width);
        $criteria->compare('height', $this->height);
        $criteria->compare('filename', $this->filename, true);
        $criteria->compare('filesize', $this->filesize);
        $criteria->compare('md5', $this->md5, true);
        $criteria->compare('author_id', $this->author_id);
        $criteria->compare('create_time', $this->create_time);

        return new CActiveDataProvider($this, array('criteria' => $criteria));
    }

    /**
     * @static
     * @param string $filePath
     * @return Image
     */
    public static function getFromFile($filePath)
    {
        $filesize = filesize($filePath);
        $md5 = md5_file($filePath);

        $model = Image::model()->findByAttributes(array(
                                                       'filesize' => $filesize,
                                                       'md5' => $md5,
                                                  ));
        if (!$model)
        {
            //Checking image file
            $image = Yii::app()->image->load($filePath);

            $extension = CFileHelper::getExtension($filePath);
            $filename = FileHelper::getUniqueFilename(Yii::app()->params['imagesPath'], $extension, Yii::app()->params['imageFilenameLength']);
            if (!$image->save(Yii::app()->params['imagesPath'] . DIRECTORY_SEPARATOR . $filename))
                return;

            $model = new Image();
            $model->width = $image->width;
            $model->height = $image->height;
            $model->filename = $filename;
            $model->filesize = $filesize;
            $model->md5 = $md5;

            $model->save();
        }

        return $model;
    }

    /**
     * @static
     * @param CUploadedFile $image
     * @return Image
     */
    public static function getFromCUploadedFile(CUploadedFile $image)
    {
        $filename = FileHelper::getUniqueFilename(sys_get_temp_dir(), $image->getExtensionName(), 8);
        $filePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $filename;
        $image->saveAs($filePath);
        return self::getFromFile($filePath);
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        return Yii::app()->params['imagesPath'] . DIRECTORY_SEPARATOR . $this->filename;
    }

    /**
     * @return string
     */
    public function getSrc()
    {
        return Yii::app()->params['imagesPath'] . "/" . $this->filename;
    }

    /**
     * @param int $width
     * @param int $height
     * @return ImagePreview
     */
    public function getPreview($width, $height)
    {
        foreach ($this->previews as $preview)
        {
            if ($preview->width == $width && $preview->height == $height)
            {
                return $preview;
            }
        }

        $previewFilename = FileHelper::getUniqueFilename(Yii::app()->params["imagePreviewsPath"], FileHelper::getExtension($this->getFilePath()), Yii::app()->params["imagePreviewFilenameLength"]);

        $image = Yii::app()->image->load($this->getFilePath());
        $image->resize($width, $height);
        $previewFilePath = Yii::app()->params["imagePreviewsPath"] . DIRECTORY_SEPARATOR . $previewFilename;
        $image->save($previewFilePath);

        $result = new ImagePreview();
        $result->image_id = $this->id;
        $result->width = $width;
        $result->height = $height;
        $result->filename = $previewFilename;
        $result->filesize = filesize($previewFilePath);
        if (!$result->save())
        {
            $result = ImagePreview::model()->findByAttributes(array(
                                                                   'image_id' => $this->id,
                                                                   'width' => $width,
                                                                   'height' => $height,
                                                              ));
        }

        $tmp = $this->previews;
        $tmp[] = $result;
        $this->previews = $tmp;

        return $result;
    }
}
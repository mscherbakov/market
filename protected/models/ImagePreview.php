<?

/**
 * @property integer $id
 * @property integer $image_id
 * @property integer $width
 * @property integer $height
 * @property string $filename
 * @property integer $filesize
 * @property integer $create_time
 *
 * @property Image $image
 */
class ImagePreview extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{image_preview}}';
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'updateAttribute' => null,
            ),
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
            ),
        );
    }

    public function rules()
    {
        return array(
            array('image_id, width, height, filename, filesize', 'required'),
            array('image_id', 'exist', 'className' => 'Image', 'attributeName' => 'id'),
            array('image_id, width, height, filesize', 'numerical', 'integerOnly' => true),
            array('filename', 'length', 'max' => 255),
            array('filename', 'unique'),
            array('image_id+width+height', 'ext.uniqueMultiColumnValidator'),

            array('id, image_id, width, height, filename, filesize, create_time', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'image' => array(self::BELONGS_TO, 'Image', 'image_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'image_id' => 'Image',
            'width' => 'Width',
            'height' => 'Height',
            'filename' => 'Filename',
            'filesize' => 'Filesize',
            'create_time' => 'Create Time',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('image_id', $this->image_id);
        $criteria->compare('width', $this->width);
        $criteria->compare('height', $this->height);
        $criteria->compare('filename', $this->filename, true);
        $criteria->compare('filesize', $this->filesize);
        $criteria->compare('create_time', $this->create_time);

        return new CActiveDataProvider($this, array('criteria' => $criteria));
    }

    public function getFilePath()
    {
        return Yii::app()->params['imagePreviewsPath'] . DIRECTORY_SEPARATOR . $this->filename;
    }

    public function getSrc()
    {
        return Yii::app()->params['imagePreviewsPath'] . "/" . $this->filename;
    }
}
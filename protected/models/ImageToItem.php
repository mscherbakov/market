<?

/**
 * @property integer $id
 * @property integer $image_id
 * @property integer $item_id
 * @property integer $author_id
 * @property integer $create_time
 *
 * @property Image $image
 * @property Item $item
 * @property User $author
 */
class ImageToItem extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{image_to_item}}';
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'updateAttribute' => null,
            ),
            'AuthorBehavior' => array(
                'class' => 'application.components.AuthorBehavior',
                'editorAttribute' => null,
            ),
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
            ),
        );
    }

    public function rules()
    {
        return array(
            array('image_id, item_id', 'required'),
            array('image_id', 'exist', 'className' => 'Image', 'attributeName' => 'id'),
            array('item_id', 'exist', 'className' => 'Item', 'attributeName' => 'id'),
            array('image_id+item_id', 'ext.uniqueMultiColumnValidator'),

            array('image_id, item_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'image' => array(self::BELONGS_TO, 'Image', 'image_id'),
            'item' => array(self::BELONGS_TO, 'Item', 'item_id'),
            'author' => array(self::BELONGS_TO, 'User', 'author_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'image_id' => 'Image',
            'item_id' => 'Item',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('image_id', $this->image_id);
        $criteria->compare('item_id', $this->item_id);

        return new CActiveDataProvider($this, array('criteria' => $criteria));
    }
}
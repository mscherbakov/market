<?

/**
 * @property integer $id
 * @property integer $category_id
 * @property integer $manufacturer_id
 * @property string $article
 * @property string $name
 * @property string $description
 * @property integer $yandex_market_model_id
 * @property integer $parse_time
 * @property integer $author_id
 * @property integer $editor_id
 * @property integer $create_time
 * @property integer $update_time
 *
 * @property Image[] $images
 * @property integer $imageCount
 * @property Category $category
 * @property Manufacturer $manufacturer
 * @property User $author
 * @property User $editor
 */
class Item extends ActiveRecord
{
    public $yandex_market_url;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{item}}';
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ),
            'AuthorBehavior' => array(
                'class' => 'application.components.AuthorBehavior',
                'setEditorOnCreate' => true,
            ),
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
            ),
        );
    }

    public function rules()
    {
        return array(
            array('category_id', 'exist', 'className' => 'Category', 'attributeName' => 'id'),
            array('category_id', 'CategoryValidator', 'skipOnError' => true),
            array('manufacturer_id', 'exist', 'className' => 'Manufacturer', 'attributeName' => 'id'),
            array('article, name', 'required'),
            array('article, name', 'length', 'max' => 255),
            array('article, name, description', 'SpacesFilter'),
            array('article', 'unique', 'caseSensitive' => false),
            //array('category_id+name', 'ext.uniqueMultiColumnValidator'),
            array('yandex_market_url', 'yandexMarketUrlValidator'),

            array('id, category_id, manufacturer_id, article, name, author_id, editor_id', 'safe', 'on' => 'search'),
        );
    }

    public function yandexMarketUrlValidator($attribute, $params)
    {
        $url = $this->$attribute;
        if (empty($url))
        {
            $this->yandex_market_model_id = null;
            return;
        }

        $urlParts = explode('?', $url);

        if (count($urlParts) == 2)
        {
            $urlParams = explode('&', $urlParts[1]);

            foreach ($urlParams as $urlParam)
            {
                $urlParamParts = explode('=', $urlParam);

                if (count($urlParamParts) == 2 && $urlParamParts[0] == 'modelid' && ($modelId = (int)$urlParamParts[1]) > 0)
                {
                    $this->yandex_market_model_id = $modelId;
                    return;
                }
            }
        }

        $this->addError($attribute, 'Неверная ссылка');
    }

    public function relations()
    {
        $relations = array(
            'images' => array(self::MANY_MANY, 'Image', 'image_to_item(item_id, image_id)'),
            'imageCount' => array(self::STAT, 'Image', 'image_to_item(item_id, image_id)'),
            'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
            'manufacturer' => array(self::BELONGS_TO, 'Manufacturer', 'manufacturer_id'),
            'author' => array(self::BELONGS_TO, 'User', 'author_id'),
            'editor' => array(self::BELONGS_TO, 'User', 'editor_id'),
            'propertyValues' => array(self::HAS_MANY, 'ItemPropertyValue', 'item_id', 'index' => 'property_id'),
            'filterValues' => array(self::HAS_MANY, 'FilterValueToItem', 'item_id'),
        	'property_groups' => array(self::HAS_MANY, 'ItemPropertyGroup', 'category_id'),
        );

        if (Yii::app() instanceof CWebApplication)
        {
            $webApplicationRelations = array(
                'marketItem' => array(self::HAS_ONE, 'MarketItem', 'item_id', 'on' => 'market_id = :marketId', 'params' => array('marketId' => Yii::app()->params['marketId'])),
            );

            return CMap::mergeArray($relations, $webApplicationRelations);
        }

        return $relations;
    }

    public function attributeLabels()
    {
        return array(
            'id' => '№',
            'category_id' => 'Категория',
            'manufacturer_id' => 'Производитель',
            'article' => 'Артикул',
            'name' => 'Наименование',
            'description' => 'Описание',
            'yandex_market_url' => 'Ссылка на Яндекс.Маркет',
            'author_id' => 'Автор',
            'editor_id' => 'Редактор',
            'create_time' => 'Создано',
            'update_time' => 'Редактировано',
        );
    }

    /**
     * SIMILAR-FILTER Возвращает список похожих товаров на данный на основании фильтра
     */
    public function similarFilter ()
    {
    	$_items_ids = array ();
    	$items_ids = array ();
    	$_ids = array ();
    	$_items = array ();
    	$w_a = array ();
    	$FilterValueToItems = FilterValueToItem::model()->findAll ("item_id=:item_id",array (":item_id"=>$this->id));
    	foreach ($FilterValueToItems as $FilterValueToItem)
    	{
    		$key = md5($FilterValueToItem->filter_value_id.$FilterValueToItem->integer_value.$FilterValueToItem->boolean_value);
    		$val = " (filter_value_id=\"{$FilterValueToItem->filter_value_id}\" OR integer_value=\"{$FilterValueToItem->integer_value}\" OR boolean_value=\"{$FilterValueToItem->boolean_value}\") ";
    		$w_a[$key] = $val;
    	}
    	
    	if (sizeof ($w_a) > 0)
    	{
	    	$sql_where = "filter_id={$FilterValueToItem->filter_id} AND (".implode (" OR ",$w_a).")";
			//print " +{$FilterValueToItem->item_id}/{$FilterValueToItem->filter_id} ";
			$_FilterValueToItems = FilterValueToItem::model()->findAll ($sql_where);
	
			if (is_array ($_FilterValueToItems))
			{
				foreach ($_FilterValueToItems as $_FilterValueToItem)
				{
					//print " *{$_FilterValueToItem->item_id}/{$_FilterValueToItem->filter_id} ";
					$items_ids[$_FilterValueToItem->item_id][$_FilterValueToItem->filter_id] = array (
						$_FilterValueToItem->filter_value_id,
						$_FilterValueToItem->integer_value,
						$_FilterValueToItem->boolean_value,
					);
				}
			}
	    	
	
	    	if (sizeof ($items_ids) > 0)
	    	{
	    		foreach ($items_ids as $id=>$v)
	    		{
	    			$e = 0;
	    			
	    			foreach ($FilterValueToItems as $pk=>$pv)
	    			{
	    				if (isset ($v[$pv->filter_id]) AND (
	    								($v[$pv->filter_id]['0'] == $pv->filter_value_id)
	    						 		AND ($v[$pv->filter_id]['1'] == $pv->integer_value)
	    						 		AND ($v[$pv->filter_id]['2'] == $pv->boolean_value)
	    				)) {
	    					$e++;
	    					//print " * ";
	    				}
	    			}
	    				
	    			$_items_ids[$id] = array (
	    					'item_id' 	=> $id,
	    					'params' 	=> $v,
	    					'similar' 	=> $e,
	    			);
	    		}
	
	    		//var_dump ($_items_ids);
	    		usort($_items_ids, array ($this, '_usersortAuto'));
	    		$i=0;
	    		foreach ($_items_ids as $v)
	    		{
	    			if ($v['item_id'] != $this->id)
	    			{
		    			$_items[$v['item_id']] = $v;
		    			$_ids[] = $v['item_id'];
		    			$i++;
		    			if ($i>5) break;
	    			}
	    		}

	    		if (sizeof ($_ids) > 0)
	    		{
	    			$items_search_str = implode (",", $_ids);
	    		
	    			$criteria=new CDbCriteria;
	    			$criteria->distinct = true;
	    			$criteria->condition="id IN({$items_search_str})";
	    			$items = self::model ()->findAll ( $criteria );
	    		
	    			foreach ($items as $v)
	    			{
	    				if (isset ($_items[$v->id]))
	    				{
	    					$_items[$v->id]['item'] = $v;
	    				}
	    			}
	    		
	    			return $_items;
	    		}
	    		
	    	}
    	}
    	return array ();
    }

	/**
	 * SIMILAR-AUTO Возвращает список похожих товаров на данный на основании указанных групп товаров
	 */  
	public function similarAuto()
	{
		$propertyGroups = ItemPropertyGroup::model()->findAll ('category_id=:category_id AND is_similar_search="1"',array (
    		':category_id'=>$this->category_id
		));

    	foreach ($propertyGroups as $propertyGroup)
    	{
			//print "<br><b> {$propertyGroup->name} {$propertyGroup->is_similar_search} / *{$propertyGroup->id}***{$this->category_id}/{$propertyGroup->category_id}</b><br>\r\n";
			$properties = array ();
    		foreach ($propertyGroup->properties as $property)
			{
				
				if (isset($this->propertyValues[$property->id]))
				{
					//print "{$property->name}/{$property->id}/{$property->group_id} = {$this->propertyValues[$property->id]->value}<br>\r\n";
					$properties[$property->id] = $this->propertyValues[$property->id]->value;
				}
				//print "{$property->name}/{$property->id}/{$property->group_id}<br>\r\n";
			}
			if (sizeof ($properties) > 0)
			{
				$items = $this->_similarAuto ($properties);
				if (is_array ($items) AND sizeof ($items) > 0)
				{
					return $items;
				}
			}
		}
		return array ();
	}

	/**
	 * SIMILAR-AUTO Метод обработки и поиска похожих товаров
	 * @param array $properties
	 */
	protected function _similarAuto ($properties)
	{
		$searchs = array ();
		foreach ($properties as $property_id=>$property_value)
		{
			$searchs[] = "(property_id=$property_id AND value='{$property_value}')";
		}
		
		if (sizeof ($searchs) > 0)
		{
			$search_str = implode ( " OR ", $searchs );
			//var_dump ($search_str);
			$items = ItemPropertyValue::model ()->findAll ($search_str);
			$items_ids = array ();
			
			foreach ($items as $item)
			{
				$items_ids[$item->item_id][$item->property_id] = $item->value;
			}

			if (sizeof ($items_ids) > 0)
			{
				foreach ($items_ids as $id=>$v)
				{
					$e = 0;
					foreach ($properties as $pk=>$pv)
					{
						if (isset ($v[$pk]) AND ($v[$pk] == $pv))
						{
							$e++;
						}
					}
					
					$_items_ids[$id] = array (
						'item_id' 	=> $id,
						'params' 	=> $v,
						'similar' 	=> $e,
					);
				}
				
				usort($_items_ids, array ($this, '_usersortAuto'));
				$i=0;
				foreach ($_items_ids as $v)
				{
					if ($v['item_id'] != $this->id)
					{
						$_items[$v['item_id']] = $v;
						$_ids[] = $v['item_id'];
						$i++;
						if ($i>5) break;
					}
				}

				$items_search_str = implode (",", $_ids);
				$criteria=new CDbCriteria;
				$criteria->distinct = true;
				$criteria->condition="id IN({$items_search_str})";
				$items = self::model ()->findAll ( $criteria );
				
				foreach ($items as $v)
				{
					if (isset ($_items[$v->id]))
					{
						$_items[$v->id]['item'] = $v;
					}
				}
				
				return $_items;
			}
		}
		return false;
	}

	/**
	 * SIMILAR-AUTO Метод сортировки для алгоритма автоматической выборки похожих товаров
	 * @param unknown_type $a
	 * @param unknown_type $b
	 */
	public function _usersortAuto ($a, $b)
	{
		if ($a['similar']==$b['similar']) return 0;
		return ($a['similar']>$b['similar']) ? -1 : 1;
	}
	
    public function search()
    {
        $criteria = $this->getDbCriteria();

        $criteria->compare('id', $this->id);
        if ($this->category_id == '0')
            $criteria->addCondition('category_id is null');
        else
            $criteria->compare('category_id', $this->category_id);
        if ($this->manufacturer_id == '0')
            $criteria->addCondition('manufacturer_id is null');
        else
            $criteria->compare('manufacturer_id', $this->manufacturer_id);
        $criteria->compare('article', $this->article, true);
        $criteria->compare('name', $this->name, true);
        if ($this->description == '0')
            $criteria->addCondition('description is null OR description = ""');
        elseif ($this->description == '1')
            $criteria->addCondition('description != ""');
        $criteria->compare('author_id', $this->author_id);
        $criteria->compare('editor_id', $this->editor_id);
        $criteria->compare('create_time', $this->create_time);
        $criteria->compare('update_time', $this->update_time);

        return new CActiveDataProvider($this, array('criteria' => $criteria));
    }

    protected function afterFind()
    {
        if ($this->yandex_market_model_id)
            $this->yandex_market_url = 'http://market.yandex.ru/model.xml?modelid=' . $this->yandex_market_model_id;

        parent::afterFind();
    }

    protected function beforeDelete()
    {
        $this->beginTransaction();

        ImageToItem::model()->deleteAllByAttributes(array('item_id' => $this->id));
        FilterValueToItem::model()->deleteAllByAttributes(array('item_id' => $this->id));

        return parent::beforeDelete();
    }

    /**
     * @static
     * @param int $categoryId
     * @param string $nameKeyword
     * @return array
     */
    public static function getMinMaxPrices($categoryId = 0, $nameKeyword = "")
    {
        $conditions = array();
        $params = array();

        $command = Yii::app()->db->createCommand()
            ->select('min(market_item_price.price) as min_price, max(market_item_price.price) as max_price')
            ->from('{{item}} item')
            ->leftJoin('{{market_item}} market_item', 'item.id = market_item.item_id AND market_item.market_id = :marketId', array('marketId' => Yii::app()->params['marketId']))
            ->leftJoin('{{market_item_price}} market_item_price', 'market_item.market_item_id = market_item_price.market_item_id AND market_item_price.market_id = :marketId AND market_item_price.price_type_id = :priceType', array(
                                                                                                                                                                                                                                     'marketId' => Yii::app()->params['marketId'],
                                                                                                                                                                                                                                     'priceType' => Yii::app()->user->getModel()->price_type_id,
                                                                                                                                                                                                                                ));
        if ($categoryId > 0)
        {
            $conditions[] = 'item.category_id = :categoryId';
            $params['categoryId'] = $categoryId;
        }

        if ($nameKeyword)
        {
            $conditions[] = 'item.name LIKE :keyword';
            $params['keyword'] = "%" . $nameKeyword . "%";
        }

        if (count($conditions) > 0)
        {
            $command->where(join(" AND ", $conditions), $params);
        }

        $queryResult = $command->queryRow();

        $result = array(
            'min_price' => floor($queryResult['min_price']),
            'max_price' => ceil($queryResult['max_price']),
        );

        return $result;
    }
}
<?

/**
 * @property integer $id
 * @property integer $group_id
 * @property string $name
 * @property integer $order
 * @property integer $author_id
 * @property integer $editor_id
 * @property integer $create_time
 * @property integer $update_time
 *
 * @property ItemPropertyGroup $group
 * @property User $author
 * @property User $editor
 * @property ItemPropertyValue[] $values
 */
class ItemProperty extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{item_property}}';
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ),
            'AuthorBehavior' => array(
                'class' => 'application.components.AuthorBehavior',
                'setEditorOnCreate' => true,
            ),
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
            ),
        );
    }

    public function rules()
    {
        return array(
            array('group_id, name', 'required'),
            array('group_id', 'exist', 'className' => 'ItemPropertyGroup', 'attributeName' => 'id'),
            array('name', 'length', 'max' => 255),
            array('name', 'SpacesFilter'),
            array('group_id+name', 'ext.uniqueMultiColumnValidator'),
            array('order', 'numerical', 'integerOnly' => true),
            array('order', 'default', 'value' => 0),

            array('id, group_id, name, author_id, editor_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'group' => array(self::BELONGS_TO, 'ItemPropertyGroup', 'group_id'),
            'author' => array(self::BELONGS_TO, 'User', 'author_id'),
            'editor' => array(self::BELONGS_TO, 'User', 'editor_id'),
            'values' => array(self::HAS_MANY, 'ItemPropertyValue', 'property_id', 'index' => 'item_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '№',
            'group_id' => 'Группа характеристик',
            'name' => 'Название',
            'order' => 'Порядок вывода',
            'author_id' => 'Автор',
            'editor_id' => 'Редактор',
            'create_time' => 'Создано',
            'update_time' => 'Редактировано',
        );
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);

        return array(
            'order' => $alias . '.order, ' . $alias . '.name',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        if ($this->group_id == '0')
            $criteria->addCondition('group_id is null');
        else
            $criteria->compare('group_id', $this->group_id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('author_id', $this->author_id);
        $criteria->compare('editor_id', $this->editor_id);
        $criteria->compare('create_time', $this->create_time);
        $criteria->compare('update_time', $this->update_time);

        return new CActiveDataProvider($this, array('criteria' => $criteria));
    }
}
<?

/**
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property integer $order
 * @property integer $author_id
 * @property integer $editor_id
 * @property integer $create_time
 * @property integer $update_time
 * * Указывает на то что по группе параметров можно проводить поиск похожих товаров
 * @property integer $is_similar_search
 *
 * @property Category $category
 * @property ItemProperty[] $properties
 * @property integer $propertyCount
 * @property User $author
 * @property User $editor
 */
class ItemPropertyGroup extends ActiveRecord
{
    private $_isDeleteProperties = false;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{item_property_group}}';
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ),
            'AuthorBehavior' => array(
                'class' => 'application.components.AuthorBehavior',
                'setEditorOnCreate' => true,
            ),
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
            ),
        );
    }

    public function rules()
    {
        return array(
            array('category_id, name', 'required'),
            array('category_id', 'exist', 'className' => 'Category', 'attributeName' => 'id'),
            array('category_id', 'CategoryValidator', 'skipOnError' => true),
            array('name', 'length', 'max' => 255),
            array('name', 'SpacesFilter'),
            array('category_id+name', 'ext.uniqueMultiColumnValidator'),
            array('order,is_similar_search', 'numerical', 'integerOnly' => true),
            array('order', 'default', 'value' => 0),

            array('id, category_id, name, author_id, editor_id, is_similar_search', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
            'properties' => array(self::HAS_MANY, 'ItemProperty', 'group_id'),
            'propertyCount' => array(self::STAT, 'ItemProperty', 'group_id'),
            'author' => array(self::BELONGS_TO, 'User', 'author_id'),
            'editor' => array(self::BELONGS_TO, 'User', 'editor_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '№',
            'category_id' => 'Категория',
            'name' => 'Название',
            'order' => 'Порядок вывода',
            'author_id' => 'Автор',
            'editor_id' => 'Редактор',
            'create_time' => 'Создано',
            'update_time' => 'Редактировано',
        	// Указывает на то что по группе параметров можно проводить поиск похожих товаров
        	'is_similar_search' => 'Определять похожие товаровы',
        );
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);

        return array(
            'order' => $alias . '.order, ' . $alias . '.name',
        );
    }

    public function scopes()
    {
    	return array(
    		'is_similar' => array(
    			'condition' => $this->getTableAlias() . '.is_similar_search = 1',
    		),
    	);
    }
    
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('category_id', $this->category_id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('author_id', $this->author_id);
        $criteria->compare('editor_id', $this->editor_id);
        $criteria->compare('create_time', $this->create_time);
        $criteria->compare('update_time', $this->update_time);
        $criteria->compare('is_similar_search', $this->is_similar_search);
        

        return new CActiveDataProvider($this, array('criteria' => $criteria));
    }

    protected function beforeDelete()
    {
        $this->beginTransaction();

        if ($this->_isDeleteProperties)
        {
            foreach ($this->properties as $property)
            {
                if (!$property->delete())
                    return false;
            }
        }
        else
        {
            $criteria = new CDbCriteria();
            $criteria->addColumnCondition(array('group_id' => $this->id));
            ItemProperty::model()->updateAll(array('group_id' => null), $criteria);
        }

        return parent::beforeDelete();
    }

    public function delete($isDeleteProperties = false)
    {
        $this->_isDeleteProperties = $isDeleteProperties;
        return parent::delete();
    }

    /**
     * @static
     * @param Category $model
     * @param array $result
     * @param int $deep
     */
    private static function getRecursiveListData(Category $model, &$result, $deep = 0)
    {
        $repeatString = "....";

        $modelString = str_repeat($repeatString, $deep) . $model->name;
        $result[$modelString] = array();

        if (count($model->children) > 0)
            foreach ($model->children as $child)
                self::getRecursiveListData($child, $result, $deep + 1);
        else
            foreach ($model->propertyGroups as $propertyGroup)
                $result[$propertyGroup->id] = str_repeat($repeatString, $deep + 1) . $propertyGroup->name;
    }

    public static function getListData()
    {
        $result = array();

        foreach (Category::getModels() as $model)
            if (!$model->parent_id)
                self::getRecursiveListData($model, $result);

        return $result;
    }
}
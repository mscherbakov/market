<?

/**
 * @property integer $id
 * @property integer $property_id
 * @property integer $item_id
 * @property string $value
 * @property integer $author_id
 * @property integer $editor_id
 * @property integer $create_time
 * @property integer $update_time
 *
 * @property Item $item
 * @property ItemProperty $property
 * @property User $author
 * @property User $editor
 */
class ItemPropertyValue extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{item_property_value}}';
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ),
            'AuthorBehavior' => array(
                'class' => 'application.components.AuthorBehavior',
                'setEditorOnCreate' => true,
            ),
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
            ),
        );
    }

    public function rules()
    {
        return array(
            array('property_id', 'exist', 'className' => 'ItemProperty', 'attributeName' => 'id'),
            array('item_id', 'exist', 'className' => 'Item', 'attributeName' => 'id'),
            array('property_id+item_id', 'ext.uniqueMultiColumnValidator'),
            array('value', 'SpacesFilter'),

            array('id, property_id, item_id, value, author_id, editor_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'item' => array(self::BELONGS_TO, 'Item', 'item_id'),
            'property' => array(self::BELONGS_TO, 'ItemProperty', 'property_id'),
            'author' => array(self::BELONGS_TO, 'User', 'author_id'),
            'editor' => array(self::BELONGS_TO, 'User', 'editor_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '№',
            'property_id' => 'Характеристика',
            'item_id' => 'Номенклатура',
            'value' => 'Значение',
            'author_id' => 'Автор',
            'editor_id' => 'Редактор',
            'create_time' => 'Создано',
            'update_time' => 'Редактировано',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('property_id', $this->property_id);
        $criteria->compare('item_id', $this->item_id);
        $criteria->compare('value', $this->value, true);
        $criteria->compare('author_id', $this->author_id);
        $criteria->compare('editor_id', $this->editor_id);
        $criteria->compare('create_time', $this->create_time);
        $criteria->compare('update_time', $this->update_time);

        return new CActiveDataProvider($this, array('criteria' => $criteria));
    }
}
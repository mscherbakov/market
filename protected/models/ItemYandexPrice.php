<?

/**
 * @property integer $id
 * @property integer $item_id
 * @property integer $price
 * @property boolean $is_warranty
 * @property string $market_link
 *
 * @property Item $item
 */
class ItemYandexPrice extends ActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{item_yandex_price}}';
    }

    public function rules()
    {
        return array(
            array('item_id, price, market_link', 'required'),
            array('item_id', 'exist', 'className' => 'Item', 'attributeName' => 'id'),
            array('is_warranty', 'boolean'),
        );
    }

    public function relations()
    {
        return array(
            'item' => array(self::BELONGS_TO, 'Item', 'item_id'),
        );
    }

    public function defaultScope()
    {
        return array(
            'order' => 'price',
        );
    }
}
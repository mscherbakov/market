<?

/**
 * @property integer $item_id
 * @property boolean $is_parse_by_article
 * @property integer $parse_time
 *
 * @property ItemYandexPrice[] $prices
 */
class ItemYandexPriceParser extends ActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{item_yandex_price_parser}}';
    }

    public function rules()
    {
        return array(
            array('id', 'exist', 'className' => 'Item', 'attributeName' => 'id'),
            array('is_parse_by_article', 'boolean'),
        );
    }

    public function relations()
    {
        return array(
            'item' => array(self::BELONGS_TO, 'Item', 'id'),
            'prices' => array(self::HAS_MANY, 'ItemYandexPrice', 'item_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'is_parse_by_article' => 'Парсить по артикулу',
            'parse_time' => 'Последний парсинг',
            'prices' => 'Цены',
        );
    }

    public function getPricesString()
    {
        $result = "";

        foreach ($this->prices as $price) {
            $result .= CHtml::link($price->price, $price->market_link) . " ";
        }

        return $result;
    }

    public static function getExportContent()
    {
        $command = Yii::app()->db->createCommand();
        $command->select('{{market_item}}.market_item_id as item_id, {{item}}.article, {{item}}.name, {{item_yandex_price}}.price, {{item_yandex_price}}.is_warranty, {{item_yandex_price}}.market_link')
            ->from('{{item_yandex_price}}')
            ->join('{{item}}', '{{item_yandex_price}}.item_id = {{item}}.id')
            ->join('{{market_item}}', '{{item_yandex_price}}.item_id = {{market_item}}.item_id')
            ->where('{{market_item}}.market_id=1')
            ->order('{{market_item}}.market_item_id, {{item_yandex_price}}.price');
        $queryResult = $command->queryAll();

        $content = "";
        $prevItemId = "";

        $i = 0;
        foreach ($queryResult as $row)
        {
            foreach ($row as $key => $value)
            {
                $row[$key] = iconv("utf-8", "cp1251//TRANSLIT", str_replace(";", " ", $value));
            }

            if ($row['item_id'] != $prevItemId)
            {
                $prevItemId = $row['item_id'];
                $i = 1;
            }
            else
            {
                $i++;
            }

            $content .= join(";", array($i, $row['item_id'], $row['article'], $row['name'], $row['price'], 643, $row['is_warranty'], $row['market_link'])) . "\n";
        }

        return $content;
    }

    public static function getExportFileName()
    {
        return "yandex_" . Yii::app()->format->formatDate(time()) . ".csv";
    }
}
<?

class LoginForm extends CFormModel
{
	private $_userIdentity;

	public $login;
	public $password;

	public function attributeLabels()
	{
		return array(
			'login' => 'Логин',
			'password' => 'Пароль',
		);
	}

	public function rules()
	{
		return array(
			array('login, password', 'required'),
			array('password', 'authenticate'),
		);
	}

	public function authenticate($attribute, $params)
	{
		$this->_userIdentity = new UserIdentity($this->login, $this->password);
		if (!$this->_userIdentity->authenticate())
		{
			$this->addError('password', 'Неверная пара логин/пароль');
		}
	}

	public function login()
	{
		if ($this->_userIdentity === null)
		{
			$this->_userIdentity = new UserIdentity($this->login, $this->password);
			$this->_userIdentity->authenticate();
		}
		if ($this->_userIdentity->errorCode === UserIdentity::ERROR_NONE)
		{
			$duration = 3600 * 24 * 30;
			Yii::app()->user->login($this->_userIdentity, $duration);
			return true;
		}
		else
			return false;
	}
}

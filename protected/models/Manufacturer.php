<?

/**
 * @property integer $id
 * @property string $name
 * @property integer $author_id
 * @property integer $editor_id
 * @property integer $create_time
 * @property integer $update_time
 *
 * @property Item[] $items
 * @property integer $itemCount
 * @property User $editor
 * @property User $author
 */
class Manufacturer extends ActiveRecord
{
    private $_isDeleteItems;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{manufacturer}}';
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ),
            'AuthorBehavior' => array(
                'class' => 'application.components.AuthorBehavior',
                'setEditorOnCreate' => true,
            ),
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
            ),
        );
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name', 'length', 'max' => 255),
            array('name', 'SpacesFilter'),
            array('name', 'unique', 'caseSensitive' => false),

            array('id, name, author_id, editor_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'items' => array(self::HAS_MANY, 'Item', 'manufacturer_id'),
            'itemCount' => array(self::STAT, 'Item', 'manufacturer_id'),
            'editor' => array(self::BELONGS_TO, 'User', 'editor_id'),
            'author' => array(self::BELONGS_TO, 'User', 'author_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '№',
            'name' => 'Название',
            'author_id' => 'Автор',
            'editor_id' => 'Редактор',
            'create_time' => 'Создано',
            'update_time' => 'Редактировано',
        );
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);

        return array(
            'order' => $alias . '.name',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('author_id', $this->author_id);
        $criteria->compare('editor_id', $this->editor_id);
        $criteria->compare('create_time', $this->create_time);
        $criteria->compare('update_time', $this->update_time);
        $criteria->with = array('author', 'editor');

        return new CActiveDataProvider($this, array('criteria' => $criteria));
    }

    protected function beforeDelete()
    {
        $this->beginTransaction();

        if ($this->_isDeleteItems)
        {
            foreach ($this->items as $item)
            {
                if (!$item->delete())
                    return false;
            }
        }
        else
        {
            $criteria = new CDbCriteria();
            $criteria->addColumnCondition(array('manufacturer_id' => $this->id));
            Item::model()->updateAll(array('manufacturer_id' => null), $criteria);
        }

        return parent::beforeDelete();
    }

    public function delete($isDeleteItems = false)
    {
        $this->_isDeleteItems = $isDeleteItems;
        return parent::delete();
    }

    public static function getListData()
    {
        return CHtml::listData(self::model()->findAll(), 'id', 'name');
    }

    /**
     * @param int $categoryId
     * @return array
     */
    public function findAllByCategoryId($categoryId)
    {
        $command = Yii::app()->db->createCommand()
            ->select('manufacturer.*')
            ->from('{{manufacturer}} manufacturer')
            ->leftJoin('{{item}} item', 'item.manufacturer_id=manufacturer.id')
            ->where('item.category_id=:categoryId', array('categoryId' => $categoryId))
            ->group('item.manufacturer_id')
            ->order('manufacturer.name');
        $queryResult = $command->queryAll();

        return $this->populateRecords($queryResult, false);
    }

    /**
     * @param string $keyword
     * @return array
     */
    public function findAllByItemKeyword($keyword)
    {
        $command = Yii::app()->db->createCommand()
            ->select('manufacturer.*')
            ->from('{{manufacturer}} manufacturer')
            ->leftJoin('{{item}} item', 'item.manufacturer_id=manufacturer.id')
            ->where('item.name LIKE :keyword', array('keyword' => '%' . $keyword . '%'))
            ->group('item.manufacturer_id')
            ->order('manufacturer.name');
        $queryResult = $command->queryAll();

        return $this->populateRecords($queryResult, false);
    }

    /**
     * @param int $categoryId
     * @param string $keyword
     * @return array
     */
    public function findAllByCategoryIdAndItemKeyword($categoryId, $keyword)
    {
        $command = Yii::app()->db->createCommand()
            ->select('manufacturer.*')
            ->from('{{manufacturer}} manufacturer')
            ->leftJoin('{{item}} item', 'item.manufacturer_id=manufacturer.id')
            ->where('item.category_id=:categoryId AND item.name LIKE :keyword', array(
                                                                                     'categoryId' => $categoryId,
                                                                                     'keyword' => '%' . $keyword . '%'
                                                                                ))
            ->group('item.manufacturer_id')
            ->order('manufacturer.name');
        $queryResult = $command->queryAll();

        return $this->populateRecords($queryResult, false);
    }
}
<?

/**
 * @property int $market_id
 * @property int $market_currency_id
 * @property string $name
 * @property double $value
 * @property int $create_time
 * @property int $update_time
 */
class MarketCurrency extends ActiveRecord
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{market_currency}}';
	}

	public function behaviors()
	{
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'setUpdateOnCreate' => true,
			),
		);
	}

	/**
	 * @param int $max_update_time
	 * @return int
	 */
	public function replicate($max_update_time)
	{
		$offset = 0;
		$limit = Yii::app()->params['b2bReplicationQueryLimit'];

		$result = $max_update_time;

		$exportFilePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . "b2bCurrency";
		$fp = fopen($exportFilePath, "w");

		do
		{
			echo time() . " Loading " . ($offset + 1) . "-" . ($offset + $limit) . " lines from b2bCurrency\n";

			$command = Yii::app()->b2bdb->createCommand()
					->select('Curr_ID, Currency, Curr_Value, UNIX_TIMESTAMP(LastUpdated) as LastUpdated')
					->from('message1062')
					->limit($limit, $offset);
			$queryResult = $command->queryAll();

			echo time() . " Lines loaded\n";

			foreach ($queryResult as $queryRow)
			{
				fputs($fp, join("\t", $queryRow) . "\n");

				if ($queryRow['LastUpdated'] > $result)
				{
					$result = $queryRow['LastUpdated'];
				}
			}

			$offset += $limit;
		}
		while (count($queryResult) > 0);

		fclose($fp);

		chmod($exportFilePath, 0777);
		
		$connection = Yii::app()->db;

		$sql = "DROP TABLE IF EXISTS `{{market_currency_temp}}`";
		$connection->createCommand($sql)->execute();

		$sql = "CREATE TEMPORARY TABLE `{{market_currency_temp}}` (
          `id` int(11) NOT NULL,
          `name` varchar(255) NOT NULL,
          `value` decimal(19, 4) NOT NULL,
          `update_time` int(11) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=INNODB DEFAULT CHARSET=utf8";
		$connection->createCommand($sql)->execute();

		$sql = "LOAD DATA INFILE '" . addslashes($exportFilePath) . "' INTO TABLE {{market_currency_temp}}";
		$connection->createCommand($sql)->execute();

		//Updating existing
		$sql = "UPDATE {{market_currency}}, {{market_currency_temp}} SET
               		{{market_currency}}.name = {{market_currency_temp}}.name,
               		{{market_currency}}.value = {{market_currency_temp}}.value,
               		{{market_currency}}.update_time = UNIX_TIMESTAMP(NOW())
			   	WHERE {{market_currency_temp}}.update_time >= :max_update_time AND
			   		{{market_currency}}.market_id = 1 AND
			   		{{market_currency}}.market_currency_id = {{market_currency_temp}}.id";
		$connection->createCommand($sql)->bindValues(array(
														  'max_update_time' => $max_update_time
													 ))->execute();

		//Updating prices
		$sql = "UPDATE {{market_item_price}}, {{market_currency_temp}} SET
               		{{market_item_price}}.price = {{market_item_price}}.currency_price * {{market_currency_temp}}.value,
               		{{market_item_price}}.update_time = UNIX_TIMESTAMP(NOW())
			   	WHERE {{market_currency_temp}}.update_time >= :max_update_time AND
			   		{{market_item_price}}.market_id = 1 AND
			   	 	{{market_item_price}}.currency_id = {{market_currency_temp}}.id";
		$connection->createCommand($sql)->bindValues(array(
														  'max_update_time' => $max_update_time
													 ))->execute();

		//Creating new
		$sql = "INSERT INTO {{market_currency}}(market_id, market_currency_id, name, value, create_time, update_time)
					SELECT 1, id, name, value, UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW())
					FROM {{market_currency_temp}}
					WHERE id NOT IN (
						SELECT market_currency_id
						FROM {{market_currency}}
						WHERE market_id = 1)";
		$connection->createCommand($sql)->execute();

		//Deleting nonexistent
		$sql = "DELETE FROM {{market_currency}}
		 		WHERE market_id = 1 AND
		 		 	market_currency_id NOT IN (
		 		 		SELECT id
		 		 		FROM {{market_currency_temp}})";
		$connection->createCommand($sql)->execute();

		return $result;
	}
}
<?

/**
 * @property int $market_id
 * @property string $market_item_id
 * @property int $item_id
 * @property string $name
 * @property string $href
 * @property int $author_id
 * @property int $editor_id
 * @property int $create_time
 * @property int $update_time
 */
class MarketItem extends ActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{market_item}}';
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ),
            'AuthorBehavior' => array(
                'class' => 'application.components.AuthorBehavior',
                'setEditorOnCreate' => true,
            ),
        );
    }

    public function rules()
    {
        return array(
            array('market_id, market_item_id, item_id, name', 'required'),
            array('market_id', 'exist', 'className' => 'Market', 'attributeName' => 'id'),
            array('item_id', 'exist', 'className' => 'Item', 'attributeName' => 'id'),
            array('name, href', 'length', 'max' => 255),
            array('market_item_id, name', 'SpacesFilter'),
        );
    }

    /**
     * @param int $max_update_time
     * @return int
     */
    public function replicate($max_update_time)
    {
        $offset = 0;
        $limit = Yii::app()->params['b2bReplicationQueryLimit'];

        $result = $max_update_time;

        $exportFilePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . "b2bItem";
        $fp = fopen($exportFilePath, "w");

        do
        {
            echo time() . " Loading " . ($offset + 1) . "-" . ($offset + $limit) . " lines from b2bItem\n";

            $command = Yii::app()->b2bdb->createCommand()
                ->select('t.Message_ID as Message_ID, t.Nomencl_ID as Nomencl_ID, t.Parent_ID as Parent_ID, s.Hidden_URL as Hidden_URL, UNIX_TIMESTAMP(t.LastUpdated) as LastUpdated')
                ->from('message1064 t')
                ->leftJoin('subdivision s', 's.Subdivision_ID=t.Subdivision_ID')
                ->limit($limit, $offset);
            $queryResult = $command->queryAll();

            echo time() . " Lines loaded\n";

            foreach ($queryResult as $queryRow)
            {
                $href = "c" . $queryRow['Parent_ID'] . "_" . $queryRow['Message_ID'] . ".html";
                if ($queryRow['Hidden_URL'])
                {
                    $href = $queryRow['Hidden_URL'] . $href;
                }
                else
                {
                    $href = "/netshop/" . $href;
                }

                fputs($fp, join("\t", array($queryRow['Nomencl_ID'], $href, $queryRow['LastUpdated'])) . "\n");

                if ($queryRow['LastUpdated'] > $result)
                {
                    $result = $queryRow['LastUpdated'];
                }
            }

            $offset += $limit;
        }
        while (count($queryResult) > 0);

        fclose($fp);

	chmod($exportFilePath, 0777);
	
        $connection = Yii::app()->db;

        $sql = "DROP TABLE IF EXISTS `{{market_item_temp}}`";
        $connection->createCommand($sql)->execute();

        $sql = "CREATE TEMPORARY TABLE `{{market_item_temp}}` (
          `id` varchar(255) NOT NULL,
          `href` varchar(255) NOT NULL,
          `update_time` int(11) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=INNODB DEFAULT CHARSET=utf8";
        $connection->createCommand($sql)->execute();

        $sql = "LOAD DATA INFILE '" . addslashes($exportFilePath) . "' INTO TABLE {{market_item_temp}}";
        $connection->createCommand($sql)->execute();

        //Updating market items href
        $sql = "UPDATE {{market_item}}, {{market_item_temp}} SET
               		{{market_item}}.href = {{market_item_temp}}.href,
               		{{market_item}}.update_time = UNIX_TIMESTAMP(NOW())
			   WHERE {{market_item_temp}}.update_time >= :max_update_time AND
			       	{{market_item}}.market_item_id = {{market_item_temp}}.id";
        $connection->createCommand($sql)->bindValues(array(
                                                          'max_update_time' => $max_update_time
                                                     ))->execute();

        return $result;
    }

    public function relations()
    {
        if (Yii::app() instanceof CWebApplication)
        {
            return array(
                //'price' => array(self::HAS_ONE, 'MarketItemPrice', array('market_id', 'market_item_id'), 'on' => 'price_type_id = :priceTypeId', 'params' => array('priceTypeId' => Yii::app()->user->getModel()->price_type_id)),
                //'availability' => array(self::HAS_ONE, 'MarketItemAvailability', array('market_id', 'market_item_id'), 'on' => 'warehouse_id = :warehouseId', 'params' => array('warehouseId' => Yii::app()->user->getModel()->warehouse_id)),
            );
        }

        return array();
    }

    /**
     * @param string $text
     * @param array $htmlOptions
     * @return string
     */
    public function getLink($text, $htmlOptions = array())
    {
        if (!$this->href)
        {
            return "";
        }

        $href = "http://" . Yii::app()->params['b2bdomen'] . $this->href;

        return CHtml::link($text, $href, $htmlOptions);
    }
}

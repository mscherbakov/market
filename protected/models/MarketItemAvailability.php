<?

/**
 * @property int $market_id
 * @property int $market_item_availability_id
 * @property string $market_item_id
 * @property int $warehouse_id
 * @property string $value
 * @property int $create_time
 * @property int $update_time
 */
class MarketItemAvailability extends ActiveRecord
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{market_item_availability}}';
	}

	public function behaviors()
	{
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'setUpdateOnCreate' => true,
			),
		);
	}

	/**
	 * @param int $max_update_time
	 * @return int
	 */
	public function replicate($max_update_time)
	{
		$offset = 0;
		$limit = Yii::app()->params['b2bReplicationQueryLimit'];

		$result = $max_update_time;

		$exportFilePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . "b2bItemAvailability";
		$fp = fopen($exportFilePath, "w");

		do
		{
			echo time() . " Loading " . ($offset + 1) . "-" . ($offset + $limit) . " lines from b2bAvailability\n";

			$command = Yii::app()->b2bdb->createCommand()
					->select('Message_ID, Nomencl_ID, Warehouse_ID, Nalichie, UNIX_TIMESTAMP(LastUpdated) as LastUpdated')
					->from('message1069')
					->limit($limit, $offset);
			$queryResult = $command->queryAll();

			echo time() . " Lines loaded\n";

			foreach ($queryResult as $queryRow)
			{
				fputs($fp, join("\t", $queryRow) . "\n");

				if ($queryRow['LastUpdated'] > $result)
				{
					$result = $queryRow['LastUpdated'];
				}
			}

			$offset += $limit;
		}
		while (count($queryResult) > 0);

		fclose($fp);

		chmod($exportFilePath, 0777);
		
		$connection = Yii::app()->db;

		$sql = "DROP TABLE IF EXISTS `{{market_item_availability_temp}}`";
		$connection->createCommand($sql)->execute();

		$sql = "CREATE TEMPORARY TABLE `{{market_item_availability_temp}}` (
          `id` int(11) NOT NULL,
          `market_item_id` varchar(255) NOT NULL,
          `warehouse_id` int(11) NOT NULL,
          `value` varchar(255) NOT NULL,
          `update_time` int(11) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=INNODB DEFAULT CHARSET=utf8";
		$connection->createCommand($sql)->execute();

		$sql = "LOAD DATA INFILE '" . addslashes($exportFilePath) . "' INTO TABLE {{market_item_availability_temp}}";
		$connection->createCommand($sql)->execute();

		//Updating existing
		$sql = "UPDATE {{market_item_availability}}, {{market_item_availability_temp}} SET
					{{market_item_availability}}.market_item_id = {{market_item_availability_temp}}.market_item_id,
					{{market_item_availability}}.warehouse_id = {{market_item_availability_temp}}.warehouse_id,
					{{market_item_availability}}.value = {{market_item_availability_temp}}.value,
					{{market_item_availability}}.update_time = UNIX_TIMESTAMP(NOW())
				WHERE {{market_item_availability_temp}}.update_time >= :max_update_time AND
					{{market_item_availability}}.market_id = 1 AND
					{{market_item_availability}}.market_item_availability_id = {{market_item_availability_temp}}.id";
		$connection->createCommand($sql)->bindValues(array(
														  'max_update_time' => $max_update_time
													 ))->execute();

		//Creating new
		$sql = "INSERT INTO {{market_item_availability}}(market_id, market_item_availability_id, market_item_id, warehouse_id, value, create_time, update_time)
					SELECT 1, id, market_item_id, warehouse_id, value, UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW())
					FROM {{market_item_availability_temp}}
					WHERE id NOT IN (
						SELECT market_item_availability_id
						FROM {{market_item_availability}}
						WHERE market_id = 1)";
		$connection->createCommand($sql)->execute();

		//Deleting nonexistent
		$sql = "DELETE FROM {{market_item_availability}}
				WHERE market_id = 1 AND
					market_item_availability_id NOT IN (
						SELECT id
						FROM {{market_item_availability_temp}})";
		$connection->createCommand($sql)->execute();

		return $result;
	}

	/**
	 * @static
	 * @param B2BItemAvailability $model
	 * @param string $alt
	 * @param array $htmlOptions
	 * @return string
	 */
	public static function image($model, $alt = '', $htmlOptions = array())
	{
		$src = "/images/catalog_count_net.gif";

		if ($model)
		{
			switch ($model->value)
			{
				case "Мало":
					$src = "/images/catalog_count_malo.gif";
					break;
				case "Средне":
					$src = "/images/catalog_count_sredne.gif";
					break;
				case "Много":
					$src = "/images/catalog_count_mnogo.gif";
					break;
			}

			if($alt == '')
			{
				$alt = $model->value;
			}
		}

		return CHtml::image($src, $alt, $htmlOptions);
	}
}
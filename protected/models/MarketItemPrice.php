<?

/**
 * @property int $market_id
 * @property int $market_item_price_id
 * @property string $market_item_id
 * @property int $price_type_id
 * @property double $currency_price
 * @property int $currency_id
 * @property double $price
 * @property int $create_time
 * @property int $update_time
 */
class MarketItemPrice extends ActiveRecord
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{market_item_price}}';
	}

	public function behaviors()
	{
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'setUpdateOnCreate' => true,
			),
		);
	}

	/**
	 * @param int $max_update_time
	 * @return int
	 */
	public function replicate($max_update_time)
	{
		$offset = 0;
		$limit = Yii::app()->params['b2bReplicationQueryLimit'];

		$result = $max_update_time;

		$exportFilePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . "b2bItemPrice";
		$fp = fopen($exportFilePath, "w");

		do
		{
			echo time() . " Loading " . ($offset + 1) . "-" . ($offset + $limit) . " lines from b2bPrice\n";

			$command = Yii::app()->b2bdb->createCommand()
					->select('Message_ID, Nomencl_ID, Price_Type_ID, Price_Value, Curr_ID, UNIX_TIMESTAMP(LastUpdated) as LastUpdated')
					->from('message1067')
					->limit($limit, $offset);
			$queryResult = $command->queryAll();

			echo time() . " Lines loaded\n";

			foreach ($queryResult as $queryRow)
			{
				fputs($fp, join("\t", $queryRow) . "\n");

				if ($queryRow['LastUpdated'] > $result)
				{
					$result = $queryRow['LastUpdated'];
				}
			}

			$offset += $limit;
		}
		while (count($queryResult) > 0);

		fclose($fp);

		chmod($exportFilePath, 0777);
		
		$connection = Yii::app()->db;

		$sql = "DROP TABLE IF EXISTS `{{market_item_price_temp}}`";
		$connection->createCommand($sql)->execute();

		$sql = "CREATE TEMPORARY TABLE `{{market_item_price_temp}}` (
          `id` int(11) NOT NULL,
          `market_item_id` varchar(255) NOT NULL,
          `price_type_id` int(11) NOT NULL,
          `currency_price` decimal(19, 4) NOT NULL,
          `currency_id` int(11) NOT NULL,
          `update_time` int(11) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=INNODB DEFAULT CHARSET=utf8";
		$connection->createCommand($sql)->execute();

		$sql = "LOAD DATA INFILE '" . addslashes($exportFilePath) . "' INTO TABLE {{market_item_price_temp}}";
		$connection->createCommand($sql)->execute();

		//Updating existing
		$sql = "UPDATE {{market_item_price}}, {{market_item_price_temp}}, {{market_currency}} SET
					{{market_item_price}}.market_item_id = {{market_item_price_temp}}.market_item_id,
					{{market_item_price}}.price_type_id = {{market_item_price_temp}}.price_type_id,
					{{market_item_price}}.currency_price = {{market_item_price_temp}}.currency_price,
					{{market_item_price}}.currency_id = {{market_item_price_temp}}.currency_id,
					{{market_item_price}}.price = {{market_item_price_temp}}.currency_price * {{market_currency}}.value,
					{{market_item_price}}.update_time = UNIX_TIMESTAMP(NOW())
				WHERE {{market_item_price_temp}}.update_time >= :max_update_time AND
					{{market_item_price}}.market_id = 1 AND
					{{market_item_price}}.market_item_price_id = {{market_item_price_temp}}.id AND
					{{market_currency}}.market_id = 1 AND
					{{market_currency}}.market_currency_id = {{market_item_price_temp}}.currency_id";
		$connection->createCommand($sql)->bindValues(array(
														  'max_update_time' => $max_update_time
													 ))->execute();

		//Creating new
		$sql = "INSERT INTO {{market_item_price}}(market_id, market_item_price_id, market_item_id, price_type_id, currency_price, currency_id, price, create_time, update_time)
					SELECT 1, {{market_item_price_temp}}.id, {{market_item_price_temp}}.market_item_id, {{market_item_price_temp}}.price_type_id, {{market_item_price_temp}}.currency_price, {{market_item_price_temp}}.currency_id, {{market_item_price_temp}}.currency_price * {{market_currency}}.value, UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW())
					FROM {{market_item_price_temp}}, {{market_currency}}
					WHERE {{market_item_price_temp}}.id NOT IN (
							SELECT market_item_price_id
							FROM {{market_item_price}}
							WHERE market_id = 1) AND
						{{market_currency}}.market_id = 1 AND
						{{market_currency}}.market_currency_id = {{market_item_price_temp}}.currency_id";
		$connection->createCommand($sql)->execute();

		//Deleting nonexistent
		$sql = "DELETE FROM {{market_item_price}}
				WHERE market_id = 1 AND
					market_item_price_id NOT IN (
						SELECT id
						FROM {{market_item_price_temp}})";
		$connection->createCommand($sql)->execute();

		return $result;
	}
}
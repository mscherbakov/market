<?

/**
 * @property int $id
 * @property string $table_name
 * @property int $max_update_time
 * @property int $update_time
 */
class MarketReplication extends ActiveRecord
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{market_replication}}';
	}

	public function behaviors()
	{
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => null,
				'setUpdateOnCreate' => true,
			),
		);
	}

	public function rules()
	{
		return array(
			array('max_update_time', 'default', 'value' => 0),
		);
	}
}

<?

/**
 * @property int $id
 * @property int $market_id
 * @property int $market_user_id
 * @property string $login
 * @property string $password
 * @property int $price_type_id
 * @property int $warehouse_id
 * @property int $create_time
 * @property int $update_time
 */
class MarketUser extends ActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{market_user}}';
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ),
        );
    }

    public function relations()
    {
        return array(
            'warehouse' => array(self::BELONGS_TO, 'MarketWarehouse', array('market_id' => 'market_id', 'warehouse_id' => 'market_warehouse_id')),
        );
    }

    public function getWarehouse()
    {
        $result = $this->getRelated('warehouse');

        if (!$result)
            $result = MarketWarehouse::model()->findByAttributes(array(
                                                                      'market_id' => Yii::app()->params['marketId'],
                                                                      'market_warehouse_id' => Yii::app()->params['defaultWarehouseId'],
                                                                 ));
        return $result;
    }

    /**
     * @param int $max_update_time
     * @return int
     */
    public function replicate($max_update_time)
    {
        $offset = 0;
        $limit = Yii::app()->params['b2bReplicationQueryLimit'];

        $result = $max_update_time;

        $exportFilePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . "b2bUser";

	$fp = fopen($exportFilePath, "w");

        do
        {
	
            echo time() . " Loading " . ($offset + 1) . "-" . ($offset + $limit) . " lines from b2bUser\n";

            $command = Yii::app()->b2bdb->createCommand()
                ->select('User_ID, Login, Password, Price_Type, Warehouse_ID, UNIX_TIMESTAMP(LastUpdated) as LastUpdated')
                ->from('user')
                ->limit($limit, $offset);
            $queryResult = $command->queryAll();

            echo time() . " Lines loaded\n";

            foreach ($queryResult as $queryRow)
            {
                if (!$queryRow['Warehouse_ID'])
                {
                    $queryRow['Warehouse_ID'] = 0;
                }

                fputs($fp, join("\t", $queryRow) . "\n");

                if ($queryRow['LastUpdated'] > $result)
                {
                    $result = $queryRow['LastUpdated'];
                }
            }

            $offset += $limit;
        }
        while (count($queryResult) > 0);

        fclose($fp);

	chmod($exportFilePath, 0777);
	
        $connection = Yii::app()->db;

        $sql = "DROP TABLE IF EXISTS `{{market_user_temp}}`";
        $connection->createCommand($sql)->execute();
	$connection->createCommand('SET SQL_BIG_TABLES=1')->execute();
	
        $sql = "CREATE TEMPORARY TABLE `{{market_user_temp}}` (
          `id` int(11) NOT NULL,
          `login` varchar(255) NOT NULL,
          `password` varchar(45) NOT NULL,
          `price_type_id` int(11) NOT NULL,
          `warehouse_id` int(11) NOT NULL,
          `update_time` int(11) NOT NULL,
          PRIMARY KEY (`id`)
        ) ";
        $connection->createCommand($sql)->execute();
	$connection->createCommand('ALTER TABLE {{market_user_temp}} MAX_ROWS=1000000000')->execute();
  
        $sql = "LOAD DATA INFILE '" . addslashes($exportFilePath) . "' INTO TABLE `{{market_user_temp}}`";
        $connection->createCommand($sql)->execute();

        //Updating existing
        $sql = "UPDATE {{market_user}}, {{market_user_temp}} SET
					{{market_user}}.login = {{market_user_temp}}.login,
					{{market_user}}.password = {{market_user_temp}}.password,
					{{market_user}}.price_type_id = {{market_user_temp}}.price_type_id,
					{{market_user}}.warehouse_id = {{market_user_temp}}.warehouse_id,
					{{market_user}}.update_time = UNIX_TIMESTAMP(NOW())
			   	WHERE {{market_user_temp}}.update_time >= :max_update_time AND
			   		{{market_user}}.market_id = 1 AND
			   		{{market_user}}.market_user_id = {{market_user_temp}}.id";
        $connection->createCommand($sql)->bindValues(array(
                                                          'max_update_time' => $max_update_time
                                                     ))->execute();

        //Creating new
        $sql = "INSERT INTO {{market_user}}(market_id, market_user_id, login, password, price_type_id, warehouse_id, create_time, update_time)
					SELECT 1, id, login, password, price_type_id, warehouse_id, UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW())
					FROM {{market_user_temp}}
					WHERE id NOT IN (
						SELECT market_user_id
						FROM {{market_user}}
						WHERE market_id = 1)";
        $connection->createCommand($sql)->execute();

        //Deleting nonexistent
        $sql = "DELETE FROM {{market_user}}
				WHERE market_id = 1 AND
					market_user_id NOT IN (
						SELECT id
						FROM {{market_user_temp}})";
        $connection->createCommand($sql)->execute();

        return $result;
    }
}
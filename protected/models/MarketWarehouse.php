<?

/**
 * @property int $market_id
 * @property int $market_warehouse_id
 * @property string $market_item_id
 * @property string $name
 * @property int $create_time
 * @property int $update_time
 */
class MarketWarehouse extends ActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{market_warehouse}}';
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ),
        );
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);

        return array(
            'order' => $alias . '.name',
        );
    }

    /**
     * @param int $max_update_time
     * @return int
     */
    public function replicate($max_update_time)
    {
        $offset = 0;
        $limit = Yii::app()->params['b2bReplicationQueryLimit'];

        $result = $max_update_time;

        $exportFilePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . "b2bWarehouse";
        $fp = fopen($exportFilePath, "w");

        do
        {
            echo time() . " Loading " . ($offset + 1) . "-" . ($offset + $limit) . " lines from b2bWarehouse\n";

            $command = Yii::app()->b2bdb->createCommand()
                ->select('Warehouse_ID, Warehouse_Descr, UNIX_TIMESTAMP(LastUpdated) as LastUpdated')
                ->from('message1072')
                ->limit($limit, $offset);
            $queryResult = $command->queryAll();

            echo time() . " Lines loaded\n";

            foreach ($queryResult as $queryRow)
            {
                fputs($fp, join("\t", $queryRow) . "\n");

                if ($queryRow['LastUpdated'] > $result)
                {
                    $result = $queryRow['LastUpdated'];
                }
            }

            $offset += $limit;
        }
        while (count($queryResult) > 0);

        fclose($fp);

	chmod($exportFilePath, 0777);
	
        $connection = Yii::app()->db;

        $sql = "DROP TABLE IF EXISTS `{{market_warehouse_temp}}`";
        $connection->createCommand($sql)->execute();

        $sql = "CREATE TEMPORARY TABLE `{{market_warehouse_temp}}` (
          `id` int(11) NOT NULL,
          `name` varchar(255) NOT NULL,
          `update_time` int(11) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=INNODB DEFAULT CHARSET=utf8";
        $connection->createCommand($sql)->execute();

        $sql = "LOAD DATA INFILE '" . addslashes($exportFilePath) . "' INTO TABLE {{market_warehouse_temp}}";
        $connection->createCommand($sql)->execute();

        //Updating existing
        $sql = "UPDATE {{market_warehouse}}, {{market_warehouse_temp}} SET
					{{market_warehouse}}.name = {{market_warehouse_temp}}.name,
					{{market_warehouse}}.update_time = UNIX_TIMESTAMP(NOW())
				WHERE {{market_warehouse_temp}}.update_time >= :max_update_time AND
					{{market_warehouse}}.market_id = 1 AND
					{{market_warehouse}}.market_warehouse_id = {{market_warehouse_temp}}.id";
        $connection->createCommand($sql)->bindValues(array(
                                                          'max_update_time' => $max_update_time
                                                     ))->execute();

        //Creating new
        $sql = "INSERT INTO {{market_warehouse}}(market_id, market_warehouse_id, name, create_time, update_time)
					SELECT 1, {{market_warehouse_temp}}.id, {{market_warehouse_temp}}.name, UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW())
					FROM {{market_warehouse_temp}}
					WHERE {{market_warehouse_temp}}.id NOT IN (
							SELECT market_warehouse_id
							FROM {{market_warehouse}}
							WHERE market_id = 1)";
        $connection->createCommand($sql)->execute();

        //Deleting nonexistent
        $sql = "DELETE FROM {{market_warehouse}}
				WHERE market_id = 1 AND
					market_warehouse_id NOT IN (
						SELECT id
						FROM {{market_warehouse_temp}})";
        $connection->createCommand($sql)->execute();

        return $result;
    }
}
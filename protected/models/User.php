<?

/**
 * @property int $id
 * @property string $login
 * @property string $password_salt
 * @property string $password_hash
 * @property boolean $is_blocked
 * @property int $author_id
 * @property int $editor_id
 * @property int $create_time
 * @property int $update_time
 * @property User $author
 * @property User $editor
 */
class User extends ActiveRecord
{
    private static $_listData;

    public $password;
    public $passwordRetry;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{user}}';
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ),
            'AuthorBehavior' => array(
                'class' => 'application.components.AuthorBehavior',
                'setEditorOnCreate' => true,
            ),
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
            ),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '№',
            'login' => 'Логин',
            'is_blocked' => 'Заблокирован',
            'author_id' => 'Автор',
            'editor_id' => 'Редактор',
            'create_time' => 'Создан',
            'update_time' => 'Редактирован',
            'password' => 'Пароль',
            'passwordRetry' => 'Повторите пароль',
        );
    }

    public function relations()
    {
        return array(
            'author' => array(self::BELONGS_TO, 'User', 'author_id'),
            'editor' => array(self::BELONGS_TO, 'User', 'editor_id'),
        );
    }

    public function rules()
    {
        return array(
            array('login', 'required', 'on' => 'insert'),
            array('login', 'length', 'max' => 255),
            array('login', 'SpacesFilter'),
            array('login', 'unique'),
            array('is_blocked', 'boolean'),
            array('is_blocked', 'default', 'value' => false),
            array('password, passwordRetry', 'required', 'on' => 'insert'),
            array('passwordRetry', 'compare', 'compareAttribute' => 'password', 'on' => 'insert'),

            array('id, login, is_blocked, author_id, editor_id', 'safe', 'on' => 'search'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.login', $this->login, true);
        $criteria->compare('t.is_blocked', $this->is_blocked);
        $criteria->compare('t.author_id', $this->author_id);
        $criteria->compare('t.editor_id', $this->editor_id);

        return new CActiveDataProvider($this, array('criteria' => $criteria));
    }

    public function beforeSave()
    {
        if ($this->password)
        {
            $this->setPassword($this->password);
        }

        return parent::beforeSave();
    }

    public function getPasswordSalt()
    {
        if (!$this->password_salt)
        {
            $this->password_salt = RandomHelper::getRandString(16);
        }

        return $this->password_salt;
    }

    public function setPassword($value)
    {
        $this->password_hash = $this->hashPassword($value);
    }

    private function hashPassword($password)
    {
        return md5($password . $this->getPasswordSalt());
    }

    public function validatePassword($password)
    {
        return $this->password_hash === $this->hashPassword($password);
    }

    public function delete()
    {
        return false;
    }

    public static function getListData()
    {
        if (!self::$_listData)
        {
            $criteria = new CDbCriteria();
            $criteria->order = 'login';

            self::$_listData = CHtml::listData(self::model()->findAll($criteria), 'id', 'login');
        }

        return self::$_listData;
    }
}

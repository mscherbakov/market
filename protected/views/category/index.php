<? $this->title = "Список категорий" ?>

<table id="main_area" width="100%" border="0" cellpadding="0" cellspacing="0" class="main_area" align="center">
    <tr>
        <td width="90%" valign="top">
            <table border="0" cellpadding="0" cellspacing="0">
                <? for ($i = 0; $i < count($models); $i += Yii::app()->params->categoriesPerRow): ?>
                <tr valign="top">
                    <? for ($j = $i; $j < count($models) && $j < $i + Yii::app()->params->categoriesPerRow; $j++): ?>
                    <td>
                        <div class="catlist_body_main">
                            <p>
                                <a class="catlist_head"
                                   href="<?= $this->createUrl('view', array('id' => $models[$j]->id)) ?>">
                                    <span class="CatsDigits">» </span>
                                    <span class="hotspot"><?= $models[$j]->name ?></span>
                                </a>
                            </p>

                            <p class="catlist_body">
                                <? foreach ($models[$j]->children as $child): ?>
                                <a class="subcats_header"
                                   href="<?= $this->createUrl('view', array('id' => $child->id)) ?>"><?= $child->name ?></a>
                                <? endforeach; ?>
                            </p>
                        </div>
                    </td>
                    <? endfor; ?>
                </tr>
                <? endfor; ?>
            </table>
        </td>
    </tr>
</table>
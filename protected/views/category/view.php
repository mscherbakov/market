<?
$this->title = "Категория " . $model->name;

$categories = array();
$tmp = $model;
while ($tmp)
{
    $categories[] = $tmp;
    $tmp = $tmp->parent;
}

$categories = array_reverse($categories);
foreach ($categories as $tmp)
{
    $this->breadcrumbLinks[$tmp->name] = $this->createUrl("view", array('id' => $tmp->id));
}
?>

<table id="main_area" width="100%" border="0" cellpadding="0" cellspacing="0" class="main_area">
    <? if (count($model->children) > 0): ?>
    <tr>
        <td width="90%" valign="top">
            <h1><?= $model->name ?> - Подкатегории</h1>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr valign="top">
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                            <? for ($i = 0; $i < count($model->children); $i += 3): ?>
                            <tr>
                                <? for ($j = $i; $j < $i + 3 && $j < count($model->children); $j++): ?>
                                <td class="CategoryName" align="center">
                                    <a class="CategoryName"
                                       href="<?= $this->createUrl("view", array('id' => $model->children[$j]->id)) ?>">
                                        <span class="CatsDigits">» </span><?= $model->children[$j]->name ?></a>
                                </td>
                                <? endfor; ?>
                            </tr>
                            <? endfor; ?>
                        </table>
                        <br>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <? else: ?>
    <? $this->widget("ItemsWidget", array('category' => $model)) ?>
    <? endif; ?>
</table>
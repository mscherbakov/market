<?

if ($model->category)
{
    $categories = array();
    $tmp = $model->category;
    while ($tmp)
    {
        $categories[] = $tmp;
        $tmp = $tmp->parent;
    }

    $categories = array_reverse($categories);
    foreach ($categories as $tmp)
    {
        $this->breadcrumbLinks[$tmp->name] = $this->createUrl("category/view", array('id' => $tmp->id));
    }
}
$this->breadcrumbLinks[$model->name] = $this->createUrl("view", array('id' => $model->id));
?>

<table id="main_area" width="100%" border="0" cellpadding="0" cellspacing="0" class="main_area">
    <tr>
        <td>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="border: 1px solid; border-color: #cccccc;" align="center">
                        <? foreach ($model->images as $image): ?>
                        <img src="<?= $image->getSrc() ?>" alt="<?= $model->name ?>">
                        <? endforeach; ?>
                    </td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">
                        <h1><?= CHtml::encode($model->name) ?></h1>
                        <? if (-1 > 0): ?>
                        <h2><?= "Цена: " . $model->price . " руб." ?></h2>
                        <? endif; ?>
                        <div><?= CHtml::encode($model->description) ?></div>
                        <? if ($model->category): ?>
                        <table>
                            <? foreach ($model->category->propertyGroups as $propertyGroup): ?>
                            <? $isGroupTrRendered = false; ?>
                            <? foreach ($propertyGroup->properties as $property): ?>
                            
                            <? if (isset($model->propertyValues[$property->id])): ?>
                                    <? if (!$isGroupTrRendered): ?>
                                        <tr>
                                            <td colspan="2"><b><?= $propertyGroup->name ?></b></td>
                                        </tr>
                                        <? $isGroupTrRendered = true; ?>
                                        <? endif; ?>
                                    <tr>
                                        <td><?= CHtml::encode($property->name) ?></td>
                                        <td><?= CHtml::encode($model->propertyValues[$property->id]->value) ?></td>
                                    </tr>
                                    <? endif; ?>
                                <? endforeach; ?>
                            <? endforeach; ?>
                        </table>
                        <? endif; ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br>
<?php if (is_array ($similars) AND sizeof ($similars) > 0) { ?> 
<table>
	<tr>
		<td>
		Похожие товары:
		</td>
	</tr>
	<tr>
		<td>
			<?php foreach ($similars as $similar_item) { ?>
				<table style="display:inline-block;width:150px;height:200px;padding:10px;">
					<tr><td style="height:20px;">&nbsp;<div style="width:150px;height:20px;overflow:hidden;position: relative;">
						<div style="width:1000px;"><a href="<?php echo Yii::app()->createUrl("item/view", array('id' => $similar_item['item']->id))?>"><?php echo $similar_item['item']->name?></a></div>
						<div style="top: 0px; position: absolute; left: 120px;"><img src="/images/alphahide.png"></div>
					</div></td></tr>
					<tr><td><a href="<?php echo Yii::app()->createUrl("item/view", array('id' => $similar_item['item']->id))?>">
						<? foreach ($similar_item['item']->images as $image): ?>
                        <img src="<?= $image->getPreview(150, 150)->getSrc(); ?>" alt="<?= $similar_item['item']->name ?>">
                        <?php break;?>
                        <? endforeach;?>
                        </a>
					</td></tr>
					<?php if (false) { ?>
					<tr><td><?php echo $similar_item['similar']?></td></tr>
					<?php } ?>
				</table>
			<?php } ?>
		</td>
	</tr>
</table>
<?php } ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" type="text/css" href="/css/stylesheet.css">
	<title><?= $this->title ?></title>
</head>
<body>
<table id="main" border="0" cellpadding="0" cellspacing="0" class="shape_box">
	<tr>
		<td><img src="/images/left-top.gif"></td>
		<td class="shape_top" width="100%"></td>
		<td><img src="/images/right-top.gif"></td>
	</tr>

	<tr>
		<td class="shape_left"></td>
		<td>
			<table id="header_area" border="0" cellpadding="5" cellspacing="5" class="header_area">
				<tbody>
				<tr>
					<td colspan="2">
						<? if (Yii::app()->user->getIsGuest()): ?>
						<a href="<?= Yii::app()->createUrl(Yii::app()->user->loginUrl[0]) ?>">Войти</a>
						<? else: ?>
						Ваш логин: <?= Yii::app()->user->name ?> <a href="<?= Yii::app()->createUrl("site/logout") ?>">Выйти</a>
						<? endif; ?>
					</td>
				</tr>
				<tr>
					<td width="283" height="133">
						<a href="/" title="Перейти на главную страницу">
							<img class="logo" src="/images/logo.gif" alt="Справочник товаров и услуг">
						</a>
					</td>
					<td width="100%" align="center" valign="top" class="search_header_box">
						<noindex>
							<form id="quick_find" action="<?= Yii::app()->createUrl("search") ?>"
								  method="get"><input type="hidden" name="r" value="search">
								<table width="100%" border="0" cellpadding="2" cellspacing="0">

									<tbody>
									<tr>
										<td class="infoBox" align="left">
											<table width="80%" border="0" cellpadding="2" cellspacing="0">
												<tbody>
												<tr>
													<td class="blockTitle">
														<table width="100%" border="0" cellpadding="2" cellspacing="0">
															<tbody>
															<tr>
																<td valign="middle" width="100%">
																	<div class="left-input">
																		<div class="right-input">
																			<div class="fill-input"><input type="text"
																										   name="keyword"
																										   value="<?= Yii::app()->request->getQuery('keyword', 'Введите текст для поиска товара или услуги') ?>"
																										   onkeyup="ajaxQuickFindUp(this);"
																										   id="quick_find_keyword"
																										   onfocus="if (this.value=='Введите текст для поиска товара или услуги') this.value=''"
																										   onblur="if (this.value=='') this.value='Введите текст для поиска товара или услуги'"
																										   size="40%">
																			</div>
																		</div>
																	</div>
																</td>
																<td valign="middle" width="10"><input type="image"
																									  src="/images/button_quick_find.gif"
																									  alt="Искать"
																									  title=" Искать ">
																</td>
															</tr>
															<tr>
																<td colspan="2" align="right" valign="top">
																	<!--a class="ext_search"
																	   href="<?= Yii::app()->createUrl("search/relatedItems") ?>">Поиск
																		картриджей и комплектующих по модели
																		устройства</a-->&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																</td>
															</tr>
															<tr>
																<td colspan="2">
																	<div class="ajaxQuickFind" id="ajaxQuickFind"
																		 style="text-align: left;"></div>
																</td>
															</tr>
															</tbody>
														</table>
													</td>
												</tr>
												</tbody>
											</table>
										</td>
									</tr>
									</tbody>
								</table>
							</form>
						</noindex>
					</td>
				</tr>
				</tbody>
			</table>
		</td>
		<td class="shape_right"></td>
	</tr>

	<tr>
		<td class="shape_left"></td>
		<td>
			<? $this->widget('zii.widgets.CBreadcrumbs', array('links' => $this->breadcrumbLinks)) ?>
			<?= $content; ?>
		</td>
		<td class="shape_right"></td>
	</tr>

	<tr>
		<td><img src="/images/left-bottom.gif"></td>
		<td class="shape_bottom"></td>
		<td><img src="/images/right-bottom.gif"></td>
	</tr>
</table>
<br>
<table id="bottom_area" border="0" cellpadding="0" cellspacing="0" class="bottom_area">
	<tr>
		<td class="bottom_area" align="left" valign="bottom"></td>
	</tr>
</table>
</body>
</html>
<?
$this->breadcrumbLinks["Результаты поиска"] = $this->createUrl("index", array('keyword' => $keyword));
if ($category)
{
	$this->breadcrumbLinks[$category->name] = $this->createUrl("index", array('keyword' => $keyword, 'categoryId' => $category->id));
}
?>

<table id="main_area" width="100%" border="0" cellpadding="0" cellspacing="0" class="main_area">
	<? $this->widget("ItemsWidget", array('keyword' => $keyword, 'category' => $category))	?>
</table>
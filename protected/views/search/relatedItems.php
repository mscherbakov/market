<?= CHtml::scriptFile("/js/search.js") ?>
<table id="main_area" width="100%" border="0" cellpadding="0" cellspacing="0" class="main_area">
	<tr>
		<td>
			<?= CHtml::beginForm($this->createUrl("relatedItems"), "get") ?>
			<?= CHtml::activeLabel($model, 'manufacturer') ?>
			<?
			$this->widget('ext.combobox.EJuiComboBox', array(
															'name' => 'manufacturer',
															'data' => ItemRelated::getItemManufacturersList(),
															'value' => $model->manufacturer,
															'options' => array(
																'allowText' => false,
																'onSelect' => 'onManufacturerSelect("' . $this->createUrl("manufacturerItems") . '")',
															),
													   ));
			?>
			<?= CHtml::activeLabel($model, 'item') ?>
			<?
			$this->widget('ext.combobox.EJuiComboBox', array(
															'name' => 'item',
															'data' => ItemRelated::getItemsList($model->manufacturer),
															'value' => $model->item,
															'options' => array(
																'allowText' => false,
															),
													   ));
			?>
			<input type="submit" value="Поиск">
			<?= CHtml::endForm() ?>
		</td>
	</tr>
	<? if (isset($criteria)): ?>
	<? $this->widget("ItemsWidget", array('criteria' => $criteria, 'header' => "Результаты поиска")) ?>
	<? endif; ?>
</table>
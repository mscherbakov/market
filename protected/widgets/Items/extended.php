<? Yii::app()->clientScript->registerPackage("jquery") ?>

<tr>
    <td width="90%">
        <?= CHtml::scriptFile("/js/filters.js") ?>﻿
        <h1><?= $this->category->name . " - Параметры" ?></h1><br>
        <?= CHtml::beginForm(Yii::app()->createUrl($this->getRoute(), $this->getActionParams()), "get") ?>

        <? foreach ($this->getFilters() as $filter): ?>
        <?
        if (!$this->getIsShowExtended() && $filter->is_extended && !$filter->getIsChecked())
            continue;
        ?>
        <div style="width: 300px; float:left; margin:0; padding: 20px">
            <p class="filters_block_name"><?= $filter->name ?></p>

            <div style="max-height: 150px; overflow-y: auto; overflow-x: hidden">
                <? if ($filter->type_id == FilterType::TYPE_STRING): ?>
                <? foreach ($filter->values as $filterValue): ?>
                    <div>
                        <?= CHtml::checkBox("", in_array($filterValue->id, $filter->getValue()), array(
                                                                                                      'id' => $filter->id . "_" . $filterValue->id,
                                                                                                      'value' => $filterValue->id,
                                                                                                      'onClick' => "filterCheck(this, '{$filter->id}')",
                                                                                                 )) ?>
                        <?= CHtml::label($filterValue->value, $filter->id . "_" . $filterValue->id) ?>
                    </div>
                    <? endforeach; ?>

                <?= CHtml::hiddenField($filter->id, join(";", $filter->getValue())) ?>
                <? endif; ?>

                <?
                if ($filter->type_id == FilterType::TYPE_INTEGER)
                {
                    $this->widget('JuiSliderInput', array(
                                                         'name' => $filter->id,
                                                         'value' => $filter->getCurrentMinValue(),
                                                         'maxValue' => $filter->getCurrentMaxValue(),
                                                         'options' => array(
                                                             'range' => true,
                                                             'min' => $filter->getMinValue(),
                                                             'max' => $filter->getMaxValue(),
                                                             'step' => pow(0.1, $filter->integer_accuracy),
                                                         ),
                                                         'htmlOptions' => array(
                                                             'size' => 6,
                                                             'style' => 'margin-bottom: 4px;',
                                                         ),
                                                    ));
                }
                elseif ($filter->type_id == FilterType::TYPE_BOOLEAN)
                {
                    echo CHtml::radioButtonList($filter->id, $filter->getValue(), array('' => 'Не важно', '1' => 'Да', '0' => 'Нет'));
                }
                ?>
            </div>
        </div>
        <? endforeach; ?>

        <div style="clear: both; text-align: center">
            <input type="image" src="/images/button_filter.gif" alt="Фильтровать" title="Фильтровать"
                   onclick="beforeFilter()">
        </div>
        <?= CHtml::endForm() ?>
    </td>
</tr>

<? Yii::app()->clientScript->registerPackage("jquery") ?>

<? if (count($this->categories) > 1): ?>
<tr>
    <td width="90%" valign="top">
        <h1>Уточните категорию товара</h1><br>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top">
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                        <? for ($i = 0; $i < count($this->categories); $i += 3): ?>
                        <tr>
                            <? for ($j = $i; $j < $i + 3 && $j < count($this->categories); $j++): ?>
                            <td class="CategoryName" align="center">
                                <a class="CategoryName"
                                   href="<?= Yii::app()->createUrl("search/index", array('categoryId' => $this->categories[$j]->id, 'keyword' => Yii::app()->request->getQuery('keyword'))) ?>">
                                    <span class="CatsDigits">» </span><?= $this->categories[$j]->name ?></a>
                            </td>
                            <? endfor; ?>
                        </tr>
                        <? endfor; ?>
                    </table>
                    <br>
                </td>
            </tr>
        </table>
    </td>
</tr>
<? endif; ?>
<tr>
    <td width="90%" valign="top">
        <?= CHtml::scriptFile("/js/filters.js") ?>﻿
        <h1><?= $this->getHeader() ?></h1><br>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <?= CHtml::beginForm(Yii::app()->createUrl($this->getRoute(), $this->getActionParams()), "get") ?>
            <tr>
                <td valign="top">
                    <table width="200" border="0" cellpadding="2" cellspacing="0">
                        <? foreach ($this->getFilters() as $filter): ?>
                        <? if (!$this->getIsShowExtended() && $filter->is_extended && !$filter->getIsChecked())
                            continue; ?>
                        <tr>
                            <td class="blockTitle">
                                <p class="filters_block_name"><?= $filter->name ?></p>

                                <? if ($filter->type_id == FilterType::TYPE_STRING): ?>
                                <? foreach ($filter->values as $filterValue): ?>
                                    <div>
                                        <?= CHtml::checkBox("", in_array($filterValue->id, $filter->getValue()), array(
                                                                                                                      'id' => $filter->id . "_" . $filterValue->id,
                                                                                                                      'value' => $filterValue->id,
                                                                                                                      'onClick' => "filterCheck(this, '{$filter->id}')",
                                                                                                                 )) ?>
                                        <?= CHtml::label($filterValue->value, $filter->id . "_" . $filterValue->id) ?>
                                    </div>
                                    <? endforeach; ?>

                                <?= CHtml::hiddenField($filter->id, join(";", $filter->getValue())) ?>
                                <? endif; ?>

                                <?
                                if ($filter->type_id == FilterType::TYPE_INTEGER)
                                {
                                    $this->widget('JuiSliderInput', array(
                                                                         'name' => $filter->id,
                                                                         'value' => $filter->getCurrentMinValue(),
                                                                         'maxValue' => $filter->getCurrentMaxValue(),
                                                                         'options' => array(
                                                                             'range' => true,
                                                                             'min' => $filter->getMinValue(),
                                                                             'max' => $filter->getMaxValue(),
                                                                             'step' => pow(0.1, $filter->integer_accuracy),
                                                                         ),
                                                                         'htmlOptions' => array(
                                                                             'size' => 6,
                                                                             'style' => 'margin-bottom: 4px;',
                                                                         ),
                                                                    ));
                                }
                                elseif ($filter->type_id == FilterType::TYPE_BOOLEAN)
                                {
                                    echo CHtml::radioButtonList($filter->id, $filter->getValue(), array('' => 'Не важно', '1' => 'Да', '0' => 'Нет'));
                                }
                                ?>
                            </td>
                            <td style="border-right: 1px dotted #999999;">&nbsp;</td>
                        </tr>
                        <? endforeach; ?>
                        <? if ($this->getIsExtendedFilters()): ?>
                        <tr>
                            <td style='text-align: center;'>
                                <?= CHtml::link('Показать все параметры', Yii::app()->createUrl($this->getRoute(), CMap::mergeArray($this->controller->actionParams, array('extended' => 1)))) ?>
                            </td>
                        </tr>
                        <? endif; ?>
                        <tr>
                            <td align="center">
                                <input type="image" src="/images/button_filter.gif" alt="Фильтровать"
                                       title="Фильтровать" onclick="beforeFilter()">
                            </td>
                        </tr>
                    </table>
                </td>

                <td valign="top">
                    <br>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="main" align="left">
                                            Сортировка:
                                            <?= $this->getSortDropDownList() ?>
                                            Товаров на странице:
                                            <? $itemsPerPageListData = array(10 => 10, 25 => 25, 50 => 50, 100 => 100) ?>
                                            <?= CHtml::dropDownList($this->pageVar, $this->getPagination()->pageSize, $itemsPerPageListData) ?>
                                            <?
                                            //Without price filter off issue #69
                                            if (false && $this->isProcessPrice): ?>
                                            <?= CHtml::label('Без учета цен', 'withoutPrice') ?>
                                            <?= CHtml::checkBox("withoutPrice", $this->isWithoutPrice) ?>
                                            <? endif; ?>
                                            <? if ($this->isProcessAvailability): ?>
                                            <?= CHtml::label('Без учета наличия', 'withoutAvailable') ?>
                                            <?= CHtml::checkBox("withoutAvailable", $this->isWithoutAvailable) ?>
                                            <? endif; ?>
                                            <input type="submit" value="Сортировать">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr valign="top">
                            <td width="70%">
                                <table width="100%" cellpaddind="0" cellspacing="5">
                                    <tr>
                                        <td>
                                            <div class="clear"></div>
                                            <div class="navigation">
                                                <? $this->widget('LinkPager', array('pages' => $this->getPagination())) ?>
                                            </div>
                                            <div class="clear"></div>
                                        </td>
                                    </tr>
                                </table>

                                <table cellspacing="2" class="tablesorter" id="US_Table">
                                    <tr height="20">
                                        <th class="tablesorter" align="center">Фото</th>
                                        <th class="tablesorter">Артикул</th>
                                        <th class="tablesorter">Наименование</th>
                                        <? if ($this->isProcessAvailability): ?>
                                        <th class="tablesorter">Наличие</th>
                                        <? endif; ?>
                                        <? if ($this->isProcessPrice): ?>
                                        <th class="tablesorter">Цена</th>
                                        <? endif; ?>
                                        <? if (Yii::app()->params['marketId']): ?>
                                        <th class="tablesorter">B2B</th>
                                        <? endif; ?>
                                    </tr>

                                    <? foreach ($this->getItems() as $item): ?>
                                    <tr>
                                        <td width="50" height="50" style="border: 1px solid; border-color: #cccccc;"
                                            align="center">
                                            <? if (count($item->images) > 0): ?>
                                            <a href="<?= Yii::app()->createUrl("item/view", array('id' => $item->id)) ?>">
                                                <img src="<?= $item->images[0]->getPreview(100, 100)->getSrc() ?>"
                                                     alt="<?= CHtml::encode($item->name) ?>">
                                            </a>
                                            <? endif; ?>
                                        </td>

                                        <td class="contents" valign="middle">
                                            <a class="ProductName"
                                               href="<?= Yii::app()->createUrl("item/view", array('id' => $item->id)) ?>"><?= CHtml::encode($item->article) ?></a>
                                        </td>

                                        <td class="contents" valign="middle">
                                            <a class="ProductName"
                                               href="<?= Yii::app()->createUrl("item/view", array('id' => $item->id)) ?>"><?= CHtml::encode($item->name) ?></a>
                                        </td>

                                        <? if ($this->isProcessAvailability): ?>
                                        <td valign="middle" align="center">
                                            <?= MarketItemAvailability::image($item->marketItem->availability) ?>
                                        </td>
                                        <? endif; ?>

                                        <? if ($this->isProcessPrice): ?>
                                        <td valign="middle" align="center">
                                            <p class="contents_price"><?= (($item->marketItem->price) ? Yii::app()->format->formatNumber($item->marketItem->price->price) . " руб." : "-") ?></p>
                                        </td>
                                        <? endif; ?>

                                        <? if (Yii::app()->params['marketId']): ?>
                                        <td>
                                            <?= $item->marketItem->getLink('Уточнить цену и заказать') ?>
                                        </td>
                                        <? endif; ?>
                                    </tr>
                                    <? endforeach; ?>
                                </table>

                                <table width="100%" cellpaddind="0" cellspacing="5">
                                    <tr>
                                        <td>
                                            <div class="clear"></div>
                                            <div class="navigation">
                                                <? $this->widget('LinkPager', array('pages' => $this->getPagination())) ?>
                                            </div>
                                            <div class="clear"></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>&nbsp;&nbsp;&nbsp;</td>
            </tr>
            <?= CHtml::endForm() ?>
        </table>
    </td>
</tr>

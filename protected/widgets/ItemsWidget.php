<?

class ItemsWidget extends Widget
{
    /**
     * @var string
     */
    public $keyword;

    /**
     * @var Category
     */
    public $category;

    /**
     * @var bool
     */
    public $isProcessAvailability = false;

    /**
     * @var bool
     */
    public $isProcessPrice = false;

    /**
     * @var string
     */
    public $pageVar = 'perPage';

    /**
     * @var string
     */
    public $sortVar = 'sort';

    /**
     * @var array()
     */
    private $_filters;

    /**
     * @var CPagination
     */
    private $_pagination;

    /**
     * @var array
     */
    private $_actionParams;

    /**
     * @var CSort
     */
    private $_sort;

    private $_sortAttributes = array('name');

    /**
     * @var CDbCriteria
     */
    private $_criteria;

    /**
     * @var bool
     */
    private $_isWithoutPrice = true;

    /**
     * @var bool
     */
    private $_isWithoutAvailable = false;

    private $_items;

    private $_categories;

    public function run()
    {
        if ($this->category && $this->getIsShowExtended() && $this->getIsExtendedFilters())
            $this->render('extended');
        else
            $this->render('index');
    }

    public function init()
    {
        $this->_filters = array();
        $this->_actionParams = $this->controller->actionParams;
        unset($this->_actionParams['x']);
        unset($this->_actionParams['y']);
        unset($this->_actionParams['extended']);

        if ($warehouseId = Yii::app()->request->getQuery('warehouseId'))
        {
            Yii::app()->user->setState('warehouseId', $warehouseId);
            unset($this->_actionParams['warehouseId']);
        }

        $this->_criteria = new CDbCriteria();

        $this->initFilters();
        if ($this->getIsShowExtended())
            return;

        $this->_criteria->select = 't.*';

        if (Yii::app()->params['marketId'])
        {
            $this->_criteria->join = 'left join {{market_item}} on t.id = {{market_item}}.item_id AND {{market_item}}.market_id = :marketId';
            $this->_criteria->params['marketId'] = Yii::app()->params['marketId'];
            $this->_criteria->addCondition("{{market_item}}.item_id is not null");

            if ($this->isProcessPrice)
            {
                $this->_criteria->join .= ' left join {{market_item_price}} on {{market_item}}.market_item_id = {{market_item_price}}.market_item_id AND {{market_item_price}}.market_id = :marketId AND {{market_item_price}}.price_type_id = :priceTypeId';
                $this->_criteria->params['priceTypeId'] = Yii::app()->user->getModel()->price_type_id;

                //Without price filter off issue #69
                /* if (Yii::app()->request->getQuery('withoutPrice') == "1")
                    $this->_isWithoutPrice = true;
                else
                    $this->_criteria->addCondition("{{market_item_price}}.price > 0"); */

                unset($this->_actionParams['withoutPrice']);

                $this->_sortAttributes['price'] = array(
                    'asc' => 'market_item_price.price',
                    'desc' => 'market_item_price.price DESC',
                    'label' => 'price'
                );
            }

            if ($this->isProcessAvailability)
            {
                if (Yii::app()->request->getQuery('withoutAvailable') == "1")
                    $this->_isWithoutAvailable = true;
                else
                {
                    $this->_criteria->join .= ' left join {{market_item_availability}} on {{market_item}}.market_item_id = {{market_item_availability}}.market_item_id AND {{market_item_availability}}.market_id = :marketId AND {{market_item_availability}}.warehouse_id = :warehouseId';
                    $this->_criteria->params['warehouseId'] = Yii::app()->user->getWarehouse()->market_warehouse_id;
                    $this->_criteria->addCondition("{{market_item_availability}}.value in ('Мало', 'Средне', 'Много')");
                }

                unset($this->_actionParams['withoutAvailable']);
            }
        }

        if ($this->keyword)
            $this->_criteria->addSearchCondition('t.name', $this->keyword);

        if ($this->category)
            $this->_criteria->addColumnCondition(array('t.category_id' => $this->category->id));

        $this->applyFilters();
        $this->initPagination();
        $this->initSort();
    }

    private function initFilters()
    {
        $this->initManufacturerFilter();
        $this->initPriceFilter();
        $this->initCustomFilters();
    }

    private function initManufacturerFilter()
    {
        if ($this->category && $this->keyword)
        {
            $manufacturers = Manufacturer::model()->findAllByCategoryIdAndItemKeyword($this->category->id, $this->keyword);
        }
        elseif ($this->category)
        {
            $manufacturers = Manufacturer::model()->findAllByCategoryId($this->category->id);
        }
        elseif ($this->keyword)
        {
            $manufacturers = Manufacturer::model()->findAllByItemKeyword($this->keyword);
        }
        else
        {
            return;
        }

        if (count($manufacturers) == 0)
            return;

        $filter = new Filter();
        $filter->id = 'manufacturer';
        $filter->name = 'Производитель';
        $filter->type_id = FilterType::TYPE_STRING;
        $filter->values = array();

        foreach ($manufacturers as $manufacturer)
        {
            $filterValue = new FilterValue();
            $filterValue->id = $manufacturer->id;
            $filterValue->value = $manufacturer->name;

            $tmp = $filter->values;
            $tmp[$filterValue->id] = $filterValue;
            $filter->values = $tmp;
        }

        $this->_filters[] = $filter;
    }

    private function initPriceFilter()
    {
        if (!$this->isProcessPrice || $this->_isWithoutPrice)
            return;

        $categoryId = 0;
        if ($this->category)
            $categoryId = $this->category->id;

        $keyword = "";
        if ($this->keyword)
            $keyword = $this->keyword;

        $itemsMinMaxPrices = Item::getMinMaxPrices($categoryId, $keyword);
        if ($itemsMinMaxPrices['min_price'] == $itemsMinMaxPrices['max_price'])
            return;

        $filter = new Filter();
        $filter->id = 'price';
        $filter->name = 'Цена';
        $filter->type_id = FilterType::TYPE_INTEGER;
        $filter->setMinValue($itemsMinMaxPrices['min_price']);
        $filter->setMaxValue($itemsMinMaxPrices['max_price']);
        $this->_filters[] = $filter;
    }

    private function initCustomFilters()
    {
        if ($this->category)
            foreach ($this->category->filters as $filter)
                $this->_filters[] = $filter;
    }

    private function applyFilters()
    {
        $subCriteria = new CDbCriteria();
        $checkedFiltersCount = 0;

        foreach ($this->_filters as $filter)
        {
            if (in_array($filter->id, array('manufacturer', 'price')))
                $filter->applyCriteria($this->_criteria);
            else
            {
                $subCriteria2 = new CDbCriteria();
                if ($filter->applyCriteria($subCriteria2))
                {
                    $checkedFiltersCount++;
                    $subCriteria->mergeWith($subCriteria2, false);
                }
            }

            unset($this->_actionParams[$filter->id]);

            if ($filter->type_id == FilterType::TYPE_INTEGER)
            {
                unset($this->_actionParams[$filter->id . '_minValue']);
                unset($this->_actionParams[$filter->id . '_maxValue']);
            }
        }

        if ($checkedFiltersCount > 0)
        {
            $this->_criteria->having = "count(filter_id) = " . $checkedFiltersCount;
            $this->_criteria->group = 't.id';
        }
        $this->_criteria->mergeWith($subCriteria);
        $this->_criteria->together = true;


    }

    public function getItems()
    {
    	if ($this->getIsShowExtended())
            return;

        if (!$this->_items)
        {
            $this->_criteria->index = 'id';
            $this->_items = Item::model()->findAll($this->_criteria);

            $criteria = new CDbCriteria();
            $criteria->together = true;
            $criteria->with = array(
                'image',
                'image.previews',
            );
            $criteria->addInCondition('item_id', array_keys($this->_items));
            $imagesToItems = ImageToItem::model()->findAll($criteria);

            foreach ($this->_items as $item)
                $item->images = array();

            foreach ($imagesToItems as $imageToItem)
            {
                $tmp = $this->_items[$imageToItem->item_id]->images;
                $tmp[] = $imageToItem->image;
                $this->_items[$imageToItem->item_id]->images = $tmp;
            }

            if (Yii::app()->params['marketId'])
            {
                $criteria = new CDbCriteria();
                $criteria->together = true;
                if ($this->isProcessAvailability)
                    $criteria->with[] = 'availability';
                if ($this->isProcessPrice)
                    $criteria->with[] = 'price';
                $criteria->addInCondition('item_id', array_keys($this->_items));
                $criteria->group = 'item_id';
                $marketItems = MarketItem::model()->findAll($criteria);

                foreach ($marketItems as $marketItem)
                {
                    $this->_items[$marketItem->item_id]->marketItem = $marketItem;
                }
            }
        }
        return $this->_items;
    }

    private function initPagination()
    {
        $pageSize = (int)Yii::app()->request->getQuery($this->pageVar, Yii::app()->params->defaultItemsPerPage);
        unset($this->_actionParams[$this->pageVar]);

        $tmpCriteria = clone $this->_criteria;
        $tmpCriteria->select = 't.id';

        $itemsCount = Item::model()->count($tmpCriteria);
        $this->_pagination = new CPagination($itemsCount);
        $this->_pagination->pageSize = $pageSize;
        $this->_pagination->applyLimit($this->_criteria);
    }

    private function initSort()
    {
        $this->_sort = new CSort('Item');
        $this->_sort->defaultOrder = 't.name';
        $this->_sort->attributes = $this->_sortAttributes;
        $this->_sort->applyOrder($this->_criteria);
        unset($this->_actionParams[$this->_sort->sortVar]);
    }

    public function getSortDropDownList()
    {
        $sortListData = array(
            'name' => 'имя (по возрастанию)',
            'name.desc' => 'имя (по убыванию)',
        );

        if ($this->isProcessPrice)
        {
            $sortListData['price'] = 'цена (по возрастанию)';
            $sortListData['price.desc'] = 'цена (по убыванию)';
        }

        $directions = $this->_sort->getDirections();
        $selectedDirection = "";
        foreach ($directions as $directionName => $isDesc)
        {
            $selectedDirection = $directionName .
                (($isDesc) ? $this->_sort->separators[1] . $this->_sort->descTag : "");
            break;
        }

        return CHtml::dropDownList($this->_sort->sortVar, $selectedDirection, $sortListData);
    }

    public function getHeader()
    {
        if ($this->keyword)
        {
            $result = "";
            if ($this->category)
            {
                $result .= $this->category->name . " - ";
            }
            $result .= "Результаты поиска";

            return $result;
        }
        elseif ($this->category)
        {
            return $this->category->name . " - Товары";
        }
    }

    public function getRoute()
    {
        return $this->getController()->getRoute();
    }

    public function getActionParams()
    {
        return $this->_actionParams;
    }

    public function getFilters()
    {
        return $this->_filters;
    }

    public function getPagination()
    {
        return $this->_pagination;
    }

    public function getIsWithoutPrice()
    {
        return $this->_isWithoutPrice;
    }

    public function getIsWithoutAvailable()
    {
        return $this->_isWithoutAvailable;
    }

    public function getCategories()
    {
        if ($this->_categories)
        {
            return $this->_categories;
        }

        if ($this->category)
        {
            return array($this->category);
        }

        $this->_categories = Category::model()->findAllByItemKeyword($this->keyword);
        return $this->_categories;
    }

    public function getIsExtendedFilters()
    {
        foreach ($this->getFilters() as $filter)
        {
            if ($filter->is_extended)
                return true;
        }

        return false;
    }

    public function getIsShowExtended()
    {
        return Yii::app()->request->getQuery('extended') == '1';
    }
}
